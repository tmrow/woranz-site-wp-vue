<?php
    if (get_field('hide_landing') && !is_user_logged_in()) {
        header("Location:" . get_home_url() . "/404");
        die();
    }
?>
<?php get_header(); ?>
<div class="wrapper" id="single-wrapper">
    <div id="content" tabindex="-1">
        <main class="site-main" id="main">
            <?php if (!has_heading()) { ?>
                <?php get_template_part('template-parts/blocks/tax-heading'); ?>
            <?php } ?>
            <?php while (have_posts()) : the_post(); ?>
                <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </article>
            <?php endwhile; // end of the loop. ?>
        </main><!-- #main -->
    </div><!-- .row -->
</div><!-- #content -->
<?php get_footer(); ?>
