<!-- Footer -->
<?php if (!is_page('pago') and !is_page('contacto') and !is_page('cotizacion') and !is_page('alquileres') and !is_page('universitaria') and !is_page('comercial')){ ?>
<footer>
    <div class="footer-xl">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/foms-logo-white.svg" width="120"
                         alt="">
                    <p>Compañía de Seguros</p>
                    <p>Rivadavia 611. Piso 5.</p>
                    <p>Ciudad Autónoma de Buenos Aires</p>
                </div>
                <div class="col-md-8">
                    <div class="contact">
                        <a href="tel:+54901160095100"><i class="fas fa-phone"></i> 011-6009-5100</a>
                        <a href="mailto:info@foms.com.ar"><i class="fas fa-envelope"></i> info@foms.com.ar</a>
                        <a href="#"><i class="fab fa-facebook-square"></i></a>
                        <a href="#"><i class="fab fa-twitter-square"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                    <div class="highlight-text">
                        <h5 class="d-none d-lg-block">
                        Somos una compañia de <span>personas trabajando con personas</span></br>
                        con el objetivo de <span>ayudarte a avanzar</span>
                        </h5>
                        <h5 class="d-block d-lg-none">
                                                Somos una compañia de <span>personas trabajando con personas</span>
                                                con el objetivo de <span>ayudarte a avanzar</span>
                                                </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
<div class="legal-footer py-4">
    <div class="container">
        <?php wp_nav_menu(
            array(
                'theme_location' => 'footer_menu',
                'container_class' => '',
                'container_id' => '',
                'menu_class' => 'nav justify-content-center mb-1',
                'fallback_cb' => '',
                'menu_id' => 'footer-menu',
                'depth' => 2,
                'walker' => new Understrap_WP_Bootstrap_Navwalker(),
            )
        ); ?>
        <div class="row justify-content-center">
            <div class="col-12">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/legal-footer.png"
                     class="d-none d-md-block img-fluid">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/legal-footer-xs.png"
                     class="d-block d-md-none img-fluid">
            </div>
            <div class="col-12 mt-4">
                <p>La entidad aseguradora dispone de un Servicio de Atención al Asegurado que atenderá las consultas y
                    reclamos que presenten los tomadores de seguros, asegurados, beneficiarios y/o derechohabientes. El
                    Servicio de Atención al Asegurado está integrado por: RESPONSABLE: Gonzalo Oyarzabal – Tel.:
                    011-6009-5100 SUPLENTE: Anabella Calderan – Tel.: 011-6009-5100. En caso de que el reclamo no haya
                    sido resuelto o haya sido desestimado, total o parcialmente, o que haya sido denegada su admisión,
                    podrá comunicarse con la Superintendencia de Seguros de la Nación por teléfono al 0800-666-8400,
                    correo electrónico a denuncias@ssn.gob.ar o formulario web a través de www.argentina.gob.ar/ssn</p>
            </div>
        </div>
    </div>

</div>
<?php } ?>
<!-- Footer -->

<?php
if (is_page('pago') || is_page('contacto') || is_page('cotizacion')) { ?>
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
<?php } ?>

<?php  if(is_page('alquileres') OR is_page('universitaria') OR is_page('comercial')) { ?>
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	<script src="<?php echo get_template_directory_uri() ?>/landing/assets/js/swiper.min.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/landing/assets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/landing/assets/js/jquery.mask.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="<?php echo get_template_directory_uri() ?>/landing/assets/js/main.js"></script>
<?php }else{ ?>
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
<?php } ?>
<!-- endbuild -->

<?php wp_footer(); ?>

</body>

</html>
