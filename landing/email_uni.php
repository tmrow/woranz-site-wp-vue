<?php 

require 'config.php';

$mail->addAddress('alquileres@foms.com.ar');



if($_POST["name"]!="" && ($_POST["email"])!="" && $_POST["phone"]!="" && $_POST["dni"]!="" && $_POST["uni"]!=""){

$name = $_POST["name"];
$dni = $_POST["dni"]; 
$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
$uni = $_POST["uni"];
$phone = $_POST["phone"];


$mail->setFrom($email, $name);
$mail->Subject = 'Contacto alquiler universitario '.$name;


$mail->AltBody = '
Nombre y apellido: '.$name.'
DNI: '.$dni.'
E-mail: '.$email.'
Facultad: '.$uni.'
Whats App: '.$phone.'
';

$mail->Body =/* '
<html>
<head>
<title>Solicitud Caución Alquiler</title>
</head>
<body>
<h4 style="color:#999; font-size:20px; font-weight:normal;">Solicitud Caución Alquiler</h4>
<hr>
<p><b style="color:#0677C4;">Nombre: </b>'.$name.'</p>
<p><b style="color:#0677C4;">DNI: </b>'.$dni.'</p>
<p><b style="color:#0677C4;">E-mail: </b>'.$email.'</p>
<p><b style="color:#0677C4;">Teléfono: </b>'.$phone.'</p>

</body>
</html>

'*/
'<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Alquileres</title>
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
    <style type="text/css">
        body {
            margin: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
        }
        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: none !important;
            font-family: sans-serif;
            font-style: normal;
            font-weight: 400;
        }
        button {
            width: 90%;
        }
        @media screen and (max-width: 500px) {
            /*styling for objects with screen size less than 500px; */
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
                font-family: sans-serif;
            }
            table {
                /* All tables are 100% width */
                width: 100%;
            }
            .footer {
                /* Footer has 2 columns each of 48% width */
                height: auto !important;
                max-width: 48% !important;
                width: 48% !important;
            }
            table.responsiveImage {
                /* Container for images in catalog */
                height: auto !important;
                max-width: 30% !important;
                width: 30% !important;
            }
            table.responsiveContent {
                /* Content that accompanies the content in the catalog */
                height: auto !important;
                max-width: 66% !important;
                width: 66% !important;
            }
            .top {
                /* Each Columnar table in the header */
                height: auto !important;
                max-width: 48% !important;
                width: 48% !important;
            }
            .catalog {
                margin-left: 0% !important;
                text-align: center;
            }
            .full {
                width: 90% !important;
            }
            .full.main {
                padding-right: 20px !important;
                padding-left: 20px !important;
            }
        }
        @media screen and (max-width: 480px) {
            /*styling for objects with screen size less than 480px; */
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
                font-family: sans-serif;
            }
            table {
                /* All tables are 100% width */
                width: 100% !important;
                border-style: none !important;
            }
            .footer {
                /* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }
            .table.responsiveImage {
                /* Container for each image now specifying full width */
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }
            .table.responsiveContent {
                /* Content in catalog  occupying full width of cell */
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }
            .top {
                /* Header columns occupying full width */
                height: auto !important;
                max-width: 100% !important;
                width: 100% !important;
            }
            .catalog {
                margin-left: 0% !important;
            }
            button {
                width: 99% !important;
            }
        }
    </style>
</head>
<body yahoo="yahoo">
<table width="100%" cellspacing="0" cellpadding="0" style="background-color: #eaeaea">
    <tbody>
    <tr>
        <!-- HTML Spacer row -->
        <td style="font-size: 0; line-height: 0;" height="20">
            <table width="100%" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="550" class="full" align="center" cellpadding="0" cellspacing="0"
                   style="background-color: white;">
                <!-- Main Wrapper Table with initial width set to 60opx -->
                <tbody>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;background-color: #003682;" height="8"></td>
                </tr>
                <tr>
                    <!-- HTML IMAGE SPACER -->
                </tr>
                </tbody>
            </table>
            <table width="550" class="full main" align="center" cellpadding="0" cellspacing="0"
                   style="padding-top: 30px; background-color: white; padding-right: 40px; padding-left:40px;">
                <tbody>
                <tr>
                    <td style="font-size: 0; text-align: center; line-height: 0;" height="20">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/header.png"
                             alt="" style="margin: 0 auto; text-align: center; max-width: 100%;"/>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style=" font-size: 19px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.42;
                                       letter-spacing: normal;
                                       text-align: left;
                                       color: #003682;" height="40">
                                      Solicitud Caución Universitaria
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" align="center" width="100%" style="margin-top: 10px"
                               class="catalog">
                            <!-- Table for catalog -->
                            <tr>
                                <td>
                                    <table class="responsive-table" width="48%" cellspacing="0" cellpadding="0"
                                           align="left" style="margin: 0px 0px 0px 0px;">
                                        <!-- Table container for each image and description in catalog -->
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table class="table.responsiveContent" width="100%" cellspacing="0"
                                                       cellpadding="0" align="left">
                                                    <!-- Table container for content -->
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size: 14px;
                                                               font-weight: normal;
                                                               font-style: normal;
                                                               font-stretch: normal;
                                                               letter-spacing: normal;
                                                               color: #4a4a4a;
                                                               line-height: 20px;">
                                                            <p>Nombre y apellido: '.$name.'<br>
                                                                <strong><span
                                                                        style="font-size: 16px;"><?php echo $ingresos; ?></span></strong>
                                                            </p>
                                                            <p>DNI: '.$dni.'<br>
                                                                <strong><span
                                                                        style="font-size: 16px;"><?php echo $importe; ?></span></strong>
                                                            </p>
                                                            <p>E-mail: '.$email.'<br>
                                                                <strong><span
                                                                        style="font-size: 16px;"><?php echo $importe; ?></span></strong>
                                                            </p>
                                                            <p>Facultad: '.$uni.'<br>
                                                                <strong><span
                                                                        style="font-size: 16px;"><?php echo $importe; ?></span></strong>
                                                            </p>
                                                            <p>Whats App: '.$phone.'<br>
                                                                <strong><span
                                                                        style="font-size: 16px;"><?php echo $importe; ?></span></strong>
                                                            </p>                                                        
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
        <!-- HTML spacer row -->
        <td style="font-size: 0; line-height: 0;" height="20">
            <table width="96%" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" align="center" width="500" style="" class="footer full">
                <!-- Table for catalog -->
                <tr>
                    <td>
                        <table class="responsive-table" width="32%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="tel:+54901160095100" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/phone.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: -4px; "/>
                                                    011 6009 5100
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="responsive-table" width="50%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="mailto:comercial@foms.com.ar" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-copy-3.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: 2px;"/>
                                                    comercial@foms.com.ar
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="responsive-table" width="15%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="https://www.foms.com.ar" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-copy-4.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: 2px;"/>
                                                    chat
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10px;
                           font-weight: normal;
                           font-style: normal;
                           font-stretch: normal;
                           line-height: normal;
                           letter-spacing: normal;
                           color: #4a4a4a;
                           text-align: center;
                           padding-top: 20px;padding-bottom: 20px;">
                        La presente cotización es de referencia, se considera un incremento semestral
                        del 15% para el alquiler y las expensas. La información indicada en esta 
                        página está sujeta a verificación comercial y crediticia de Foms Cia. 
                        Argentina de Seguros SA y a la presentación de documentación respaldatoria.<br><br>
                        <small>v.001</small>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>';
if(!$mail->send()) {
  }else{
    echo "2";
  }
} else {
  echo "3";
}
?>