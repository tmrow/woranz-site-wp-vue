<?php /* Template Name: caucion-alquiler */ ?>

<?php get_header(); ?>

<section id="nav-menu">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-6">
                <a class="logo" href="https://www.foms.com.ar/"><img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/logo.svg" alt=""></a>
                <a class="cia" href="#">COMPAÑIA DE SEGUROS</a>
            </div>
            <div class="comunication col-6 d-flex justify-content-end">
                <div class="dropdown">
                    <a class="btn btn-foms dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        OTROS ALQUILERES
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="<?php echo home_url() ?>/universitaria">Caución Alquiler Universitario</a>
                        <a class="dropdown-item" href="<?php echo home_url() ?>/comercial">Caución Alquiler Comercial</a>
                    </div>
                </div>

                <a class="telephone d-none d-lg-block" href="tel:+541160095100" onclick="return gtag_report_conversion_tel('tel:+541160095100')" target="blank"><i class="fas fa-phone"></i> 6009-5100</a>
                <a class="whatsapp d-none d-lg-block" href="https://web.whatsapp.com/send?phone=+5491167572460" onclick="return gtag_report_conversion_wp('https://web.whatsapp.com/send?phone=+5491167572460')" target="blank"><i class="fab fa-whatsapp"></i> 11 6757-2460 </a>
            </div>
        </div>
    </div>
</section>

<section class="cotization">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-5 form-head animated fadeIn">
                <h2>Seguro de caución para alquiler de vivienda</h2>
                <div class="display">$ 15.000</div>
                <h4>Precio final, sin vueltas ni cotizaciones.</h4>
                <p class="little">Solo para alquileres de hasta $ 35.000 por mes. <a class="blue"> Promoción exclusiva hasta el 31 de diciembre de 2019. </a>Los precios y las coberturas indicadas en esta página están sujetas a verificación comercial y crediticia de Foms Cia. Argentina de Seguros SA y a la presentación de documentación respaldatoria. </p>
                <a class="btn btn-foms btn-order" href="#contact-form">PEDILO AHORA</a>
            </div>

            <div class="col-lg-6 formulario animated fadeIn">
            <form id="contact-form" action="<?php echo get_template_directory_uri() ?>/landing/email.php" method="post" data-abide='ajax' data-toggle="validator" role="form" class="needs-validation" novalidate>

                  <fieldset id="fieldset">
                    <input id="landing-type" type="hidden" value="rental">
                    <h5 class="text-center">¡Completá para que nos pongamos en contacto!</h5>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control border" id="form_name" placeholder="Tu nombre" required>
                        <div class="help-block with-errors"></div>
                        <div class="invalid-feedback">
                           <p class="invalid">Ingresá tu nombre</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input name="dni" type="number" class="form-control border" id="form_dni" placeholder="Tu DNI" required>
                        <div class="help-block with-errors"></div>
                        <div class="invalid-feedback">
                           <p class="invalid">Ingresá tu DNI</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control border" id="form_email" placeholder="Tu email" required>
                        <div class="help-block with-errors"></div>
                        <div class="invalid-feedback">
                            <p class="invalid">Ingresá tu email</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control border" id="form_phone" placeholder="Teléfono" required>
                        <div class="help-block with-errors"></div>
                        <div class="invalid-feedback">
                          <p class="invalid">Ingresá tu teléfono</p>
                        </div>
                    </div>
                    <div id="messages" class="success">
                    </div>
                    <div class="form-group">
                        <input id="submit" type="submit" class="btn btn-foms" value="PEDÍ TU SEGURO">
                    </div>
                  </fieldset>
                  <div id="sended" class="text-center d-none">
                        <img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/check.png" alt="">
                        <h4>¡Recibimos tus datos!</h4>
                        <p>Si te quedan dudas o necesitas hablar ahora podes llamarnos</p>
                        <a class="whatsapp d-none d-md-block" href="https://web.whatsapp.com/send?phone=+5491167572460" onclick="return gtag_report_conversion_wp('https://web.whatsapp.com/send?phone=+5491167572460')" target="blank"><i class="fab fa-whatsapp"></i> 11 6757-2460 </a>
                        <a class="whatsapp d-block d-md-none" href="whatsapp://send?phone=+5491167572460" target="blank" onclick="return gtag_report_conversion_wp('whatsapp://send?phone=+5491167572460')"><i class="fab fa-whatsapp"></i> 11 6757-2460 </a></a>
                        <a class="telephone" href="tel:+541160095100" onclick="return gtag_report_conversion_tel('tel:+541160095100')" target="blank"><i class="fas fa-phone"></i> 6009-5100</a>
                  </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="img-separator animated fadeInUp">
    <img class="d-none d-md-block" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/background.jpg" alt="">
    <img class="d-block d-md-none" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/background-sm.jpg" alt="">
</section>

<section id="cover" class="cover animated dissapear">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Cobertura del seguro</h2>
            </div>
            <div class="col-md-4 cover-card-cont">
                <div class="cover-card ">
                    <img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/months.svg" alt="">
                    <p>El pago del alquiler mensual hasta la suma total del contrato.</p>
                </div>
                <p id="paint" class="btn btn-foms">INCLUIDO</p>
            </div>
            <div class="col-md-4 cover-card-cont">
                <div class="cover-card">
                    <img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/pay.svg" alt="">
                    <p>El pago de expensas y ABL. Hasta la suma asegurada de $ 30.000.</p>
                </div>
                <p class="btn btn-foms">INCLUIDO</p>
            </div>
            <div class="col-md-4 cover-card-cont last">
                <div class="cover-card last">
                    <img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/paint.svg" alt="">
                    <p>Aseguramos la devolución del inmueble en las mismas condiciones en que fue entregado.</p>
                </div>
                <a class="btn btn-consult" href="#contact-form">CONSULTAR</a>
            </div>
        </div>
    </div>
</section>

<section id="orders" class="order animated dissapear">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>¿Cómo obtengo el seguro?</h2>
            </div>
            <div class="col-12 left">
                <div class="left reverse">
                    <div class="number">
                        <p class="one">1</p>
                    </div>
                    <div class="circle-container">
                    <img class="circle-img" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/step1.svg" alt="">
                        <div class="circle">
                            <div class="circle-overlay cellphone"></div>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-lg-5">
                    <h4>Completás el formulario con tus datos</h4>
                    <p>Solo pedimos tu CUIT y datos de contacto para validar la situación crediticia y darte respuesta.</p>
                </div>
            </div>
            <div id="stroke-first" class="offset-1 col-lg-4">
                <div class="stroke-container">
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-back stroke" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#dedede" stroke-width="5"/>
                    </svg>
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-color first stroke" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#FFD78F" stroke-width="5"/>
                    </svg>
                </div>
            </div>
            <div class="col-12 right">
                <div class="number">
                    <p class="two">2</p>
                </div>
                <div class="circle-container">
                <img class="circle-img" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/step2.svg" alt="">
                    <div class="circle">
                        <div class="circle-overlay second"></div>
                    </div>
                </div>
                <div class="col-10 col-lg-5">
                    <h4>Nos envías tu documentación</h4>
                    <p>Verificamos tus datos y te pedimos documentación que acredite los ingresos del grupo familiar (recibos de sueldo, facturación u otros). </p>
                </div>
            </div>
            <div id="stroke-second" class="offset-1 col-lg-4">
                <div class="stroke-container">
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-back stroke stroke-right" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#dedede" stroke-width="5"/>
                    </svg>
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-color second stroke stroke-right" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#FFD78F" stroke-width="5"/>
                    </svg>
                </div>
            </div>
            <div class="col-12 left">
                <div class="left reverse">
                    <div class="number">
                        <p class="two">3</p>
                    </div>
                    <div class="circle-container">
                        <img class="circle-img3" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/step3.svg" alt="">
                        <div class="circle">
                            <div class="circle-overlay third"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <h4>Te pedimos la copia del contrato de alquiler</h4>
                    <p>Con una copia preliminar del contrato de alquiler podemos comenzar a emitir la póliza en día.</p>
                </div>
            </div>
            <div id="fourth" class="offset-1 col-lg-4">
                <div class="stroke-container">
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-back stroke" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#dedede" stroke-width="5"/>
                    </svg>
                    <svg width="100%" height="auto" viewBox="0 0 296 157" class="stroke-color third stroke" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0V58.706C3 69.7517 11.9543 78.706 23 78.706H273C284.046 78.706 293 87.6603 293 98.706V157"
                            stroke="#FFD78F" stroke-width="5"/>
                    </svg>
                </div>
            </div>
            <div class="col-12 right">
                <div class="number">
                    <p class="two">4</p>
                </div>
                <div class="circle-container">
                    <img class="circle-img4" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/step4.svg" alt="">
                    <div class="circle">
                        <div class="circle-overlay four"></div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <h4>¡Venís a retirar tu póliza!</h4>
                    <p>Podés pagarlo online o en nuestras sucursales con tarjeta de crédito o débito</p>
                </div>
            </div>
                <div id="last-line" class="offset-6 col-6 stroke-container stroke-final">
                    <svg width="6" height="158" viewBox="0 0 6 158" fill="none" class="stroke-back stroke" keyPoints="1;0" keyTimes="0;1" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.99351 157L3.5 1" stroke="#dedede" stroke-width="5"/>
                    </svg>
                    <svg width="6" height="158" viewBox="0 0 6 158" fill="none" class="stroke-color final active stroke" keyPoints="1;0" keyTimes="0;1" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.99351 157L3.5 1" stroke="#FFD78F" stroke-width="5"/>
                    </svg>
                </div>
            <div class="col-12 center">
                <div id="final" class="final">
                    <img class="circle-img-final" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/finalstep.svg" alt="">
                </div>
            </div>
            <div class="col-12 center simple">
                <h5>¡Asi de simple, sin cotizaciones ni letras chicas!</h5>
            </div>
            <div class="col-12 center simple animated btn-sell dissapear">
                <a class="btn btn-foms" href="#contact-form">PEDILO AHORA</a>
            </div>

        </div>
    </div>
</section>

<section id="sales" class="sales">
    <div class="container float-card-uni">
        <div class="row">
            <div class="card card-dudas animated dissapear">
                <div class="card-body">
                    <h2>¿Te quedan dudas?</h2>
                    <p>Podés comunicarte con nosotros y hablar con alguno de nuestros vendedores</p>
                    <a class="whatsapp d-none d-md-block" href="https://web.whatsapp.com/send?phone=+5491167572460" onclick="return gtag_report_conversion_wp('https://web.whatsapp.com/send?phone=+5491167572460')" target="blank"><i class="fab fa-whatsapp"></i> 11 6757-2460 </a>
                    <a class="whatsapp d-block d-md-none" href="whatsapp://send?phone=+5491167572460" onclick="return gtag_report_conversion_wp('whatsapp://send?phone=+5491167572460')" target="blank"><i class="fab fa-whatsapp"></i> 11 6757-2460 </a>
                    <a class="telephone" href="tel:+541160095100" target="blank"><i class="fas fa-phone"></i> 6009-5100</a>
                    <img class="sales-uni-img" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/dudas.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="clients" class="clients dissapear animated">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Qué dicen nuestros clientes</h2>
            </div>
        </div>
    </div>
    <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="card" >
                            <h5 class="card-title">"Me atendieron y cotizaron rápido, pude alquilar sin problemas". </h5>
                            <p class="card-text">Paloma, 35 años. Inquilina.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card" >
                            <h5 class="card-title">"Presenté el contrato y en el mismo día tuve el seguro."</h5>
                            <p class="card-text">Marcos, 41 años. Inquilino.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card" >
                            <h5 class="card-title">"Los únicos que escucharon mi situación, gracias!".</h5>
                            <p class="card-text">Graciela, 55 años. Inquilina.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card" >
                            <h5 class="card-title">"Responden al toque y te ayudan a avanzar."</h5>
                            <p class="card-text">Pablo, 28 años. Inquilino.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card" >
                            <h5 class="card-title">"Llegue de Salta con mi familia y se hacia difícil alquilar, me orientaron y ayudaron para conseguirlo".</h5>
                            <p class="card-text">Luis, 52 años. Inquilino.</p>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
</section>

<section class="social">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center">
                <div class="d-none d-md-block">
                    <a href="https://www.foms.com.ar/"><img src="<?php echo get_template_directory_uri() ?>/landing/assets/img/logo_w.svg" alt=""></a>
                </div>
            </div>
            <div class="col-md-6 media">
                <a class="telephone" href="tel:+541160095100" onclick="return gtag_report_conversion_tel('tel:+541160095100')" target="blank"><i class="fas fa-phone"></i> 6009-5100</a>
                <a href="https://www.facebook.com/fomsargentina" target="blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://twitter.com/fomsargentina" target="blank"><i class="fab fa-twitter-square"></i></a>
                <a href="https://www.instagram.com/fomsargentina/" target="blank"><i class="fab fa-instagram"></i></a>
                <a class="whatsapp d-none d-md-block" href="https://web.whatsapp.com/send?phone=+5491167572460" onclick="return gtag_report_conversion_wp('https://web.whatsapp.com/send?phone=+5491167572460')" target="blank"><i class="fab fa-whatsapp"></i></a>
                <a class="whatsapp d-block d-md-none" href="whatsapp://send?phone=+5491167572460" target="blank" onclick="return gtag_report_conversion_wp('whatsapp://send?phone=+5491167572460')"><i class="fab fa-whatsapp"></i></a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="col-12 py-3">
            <img class="img-fluid d-none d-lg-block" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/legal.png" alt="">
            <img class="img-fluid d-none d-md-block d-lg-none" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/legal-md.png" alt="">
            <img class="img-fluid d-block d-md-none" src="<?php echo get_template_directory_uri() ?>/landing/assets/img/legal-sm.png" alt="">
        </div>
        <nav class="nav mb-4 ml-0 pl-0 justify-content-center">
            <a class="nav-link text-dark small" href="<?php echo home_url() ?>/politica-de-privacidad/">Política de privacidad</a>
            <a class="nav-link text-dark small" href="<?php echo home_url() ?>/resolucion-fraude/">Fraude</a>
            <a class="nav-link text-dark small" href="<?php echo home_url() ?>/contactanos">Contacto</a>
        </nav>
        <div class="col-12">
            <p>La entidad aseguradora dispone de un Servicio de Atención al Asegurado que atenderá las consultas y reclamos que presenten los tomadores de seguros, asegurados, beneficiarios y/o derechohabientes.</p>
            <p>El Servicio de Atención al Asegurado está integrado por:</p>
            <p>RESPONSABLE: Gonzalo Oyarzabal – Tel.: 11-6009-5100</p>
            <p>SUPLENTE: Anabella Calderan – Tel.: 11-6009-5100</p>
            <p>En caso de que el reclamo no haya sido resuelto o haya sido desestimado, total o parcialmente, o que haya sido denegada su admisión, podrá comunicarse con la Superintendencia de Seguros de la Nación por teléfono al 0800-666-8400, correo electrónico a denuncias@ssn.gob.ar o formulario web a través de www.argentina.gob.ar/ssn</p>
        </div>
    </div>
</footer>

<?php get_footer(); ?>
