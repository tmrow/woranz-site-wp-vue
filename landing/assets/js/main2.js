
$('#form_cuit').mask("99-99999999-9");

$('a').click(function(){
  $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
  }, 500);
  return false;
});


  //------------------- Validation-form -------------------

(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          event.preventDefault();
          if (form.checkValidity() === false) {
            event.stopPropagation();
          }else{
            var formu = $('#contact-form');
            var formMessages = $('#messages');
            var formData = $(formu).serialize();
            var fields = $('#fieldset');
            var sended = $('#sended');

            $('#submit').text('ENVIANDO...');

            $.ajax({
              type: 'POST',
              url: formu.attr('action'),
              data: formData
          }).done(function (resp) {
              
  		gtag('event', 'conversion', {'send_to': 'AW-799277376/LJYVCKfCzKYBEMCCkP0C'});


              if(resp==3){
                $(formMessages).removeClass('alert alert-success');
                $(formMessages).addClass('alert alert-danger');
                $(formMessages).text("Complete todos los campos correctamente");
              } else if(resp==2){
              $(sended).removeClass('d-none');
              $(fields).addClass('d-none');
              }
              $('#submit').text('PEDÍ TU SEGURO');
          }).fail(function (data) {
              $(formMessages).removeClass('alert alert-success');
              $(formMessages).addClass('alert alert-danger');
              $(formMessages).text("No se pudo enviar su mensaje");
              $('#submit').text('PEDÍ TU SEGURO');
          });
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();

//------------------- document.ready -------------------

$( document ).ready(function() {


//------------------- Swiper -------------------

    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3.5,
        spaceBetween: 20,
        centeredSlides: true,
        loop: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        breakpoints: {
          1024: {
              slidesPerView: 2.5,
              spaceBetween: 20,
          },
          768: {
              slidesPerView: 2,
              spaceBetween: 20,
          },
          640: {
              slidesPerView: 2,
              spaceBetween: 20,
          },
          500: {
              centeredSlides: true,
              slidesPerView: 1.2,
              spaceBetween: 15,
          }
      }
      });

//------------------- staff random -------------------

    var random = Math.floor(Math.random() * 1000);

        var $staff = $(".staff");
        $staff.eq(random % $staff.length).addClass("active");

//------------------- Waypoints -------------------

    var waypoint = new Waypoint({
      element: document.getElementById('contact-form'),
      handler: function() {
        $('.cover').addClass('fadeInUp');
        $('.cover').removeClass('dissapear');
      },
    })
    var waypoint = new Waypoint({
      element: document.getElementById('cover'),
      handler: function() {
        $('.order').addClass('fadeIn');
        $('.order').removeClass('dissapear');
      },
    })
    var waypoint = new Waypoint({
      element: document.getElementById('final'),
      handler: function() {
        $('.staff.active').addClass('fadeInLeft');
        $('.staff.active').removeClass('dissapear');
      },
    })
    var waypoint = new Waypoint({
      element: document.getElementById('final'),
      handler: function() {
        $('.card-dudas').addClass('fadeInRight');
        $('.card-dudas').removeClass('d-none');
      },
    })
    var waypoint = new Waypoint({
      element: document.getElementById('last-line'),
      handler: function() {
        $('.btn-sell').addClass('bounceIn');
        $('.btn-sell').removeClass('dissapear');
      },
    })
    var waypoint = new Waypoint({
      element: document.getElementById('sales'),
      handler: function() {
        $('.clients').addClass('fadeIn');
        $('.clients').removeClass('dissapear');
      },
    })


    var waypoint = new Waypoint({
        element: document.getElementById('paint'),
        handler: function() {
          $('.cellphone').addClass('active');
        },
    })

    var waypoint = new Waypoint({
        element: document.getElementById('orders'),
        handler: function() {
          $('.first').addClass('active');
        },
    })
    var waypoint = new Waypoint({
        element: document.getElementById('stroke-first'),
        handler: function() {
          $('.second').addClass('active');
        },
    })
    var waypoint = new Waypoint({
        element: document.getElementById('stroke-second'),
        handler: function() {
          $('.third').addClass('active');
        },
    })
    var waypoint = new Waypoint({
        element: document.getElementById('fourth'),
        handler: function() {
          $('.four').addClass('active');
        },
    })
});
