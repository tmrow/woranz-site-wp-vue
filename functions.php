<?php
/**
 * Woranz only works in WordPress 4.4 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.4-alpha', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

$woranz_includes = array(
    '/class-wp-bootstrap-navwalker.php',
);

foreach ($woranz_includes as $file) {
    $filepath = locate_template('inc' . $file);
    if (!$filepath) {
        trigger_error(sprintf('Error locating /inc%s for inclusion', $file), E_USER_ERROR);
    }
    require_once $filepath;
}

if (!function_exists('woranz_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * Create your own woranz_setup() function to override in a child theme.
     *
     * @since Woranz 1.0
     */
    function woranz_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/woranz
         * If you're building a theme based on Woranz, use a find and replace
         * to change 'woranz' to the name of your theme in all the template files
         */
        load_theme_textdomain('woranz');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for custom logo.
         *
         *  @since Woranz 1.2
         */
        add_theme_support('custom-logo', array(
            'height' => 240,
            'width' => 240,
            'flex-height' => true,
        ));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'woranz'),
            'social' => __('Social Links Menu', 'woranz'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        //add_editor_style( array( 'css/editor-style.css', woranz_fonts_url() ) );

        // Indicate widget sidebars can use selective refresh in the Customizer.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // woranz_setup
add_action('after_setup_theme', 'woranz_setup');

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Woranz 1.0
 */
function woranz_content_width()
{
    $GLOBALS['content_width'] = apply_filters('woranz_content_width', 840);
}

add_action('after_setup_theme', 'woranz_content_width', 0);

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Woranz 1.0
 */
function woranz_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar', 'woranz'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'woranz'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Content Bottom 1', 'woranz'),
        'id' => 'sidebar-2',
        'description' => __('Appears at the bottom of the content on posts and pages.', 'woranz'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Content Bottom 2', 'woranz'),
        'id' => 'sidebar-3',
        'description' => __('Appears at the bottom of the content on posts and pages.', 'woranz'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'woranz_widgets_init');


/**
 * Enqueues scripts and styles.
 *
 * @since Woranz 1.0
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 * @since Woranz 1.0
 *
 */


/**
 * Custom template tags for this theme.
 */
//require get_template_directory() . '/inc/template-tags.php';

/**
 *
 * /**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @param array $attr Attributes for the image markup.
 * @param int $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 * @since Woranz 1.0
 *
 */
function woranz_post_thumbnail_sizes_attr($attr, $attachment, $size)
{
    if ('post-thumbnail' === $size) {
        is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
        !is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
    }
    return $attr;
}

add_filter('wp_get_attachment_image_attributes', 'woranz_post_thumbnail_sizes_attr', 10, 3);

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 * @since Woranz 1.1
 *
 */



add_post_type_support('page', 'excerpt');

function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    $excerpt = strip_tags($excerpt);

    return $excerpt;
}

function excerptclean($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function cutext($text, $limit)
{
    $excerpt = explode(' ', $text, $limit);
    array_pop($excerpt);
    $excerpt = implode(" ", $excerpt) . '...</p>
		<p class="mas-info small"><a class="btn btn-link btn-sm text-danger" href="' . get_the_permalink() . '">SEGUÍ LEYENDO</a></p>';
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit)
{
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = strip_tags(implode(" ", $content)) . '...</p><p class="small mas-info"><a href="' . get_the_permalink() . '"><b> SEGUÍ LEYENDO</b></a>';
    } else {
        $content = strip_tags(implode(" ", $content)) . '...</p><p class="small mas-info"><a href="' . get_the_permalink() . '"><b> MSEGUÍ LEYENDO</b></a>';
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

add_image_size('slide_full', 1920, 1080); // Soft Crop Mode

// disable acf css on front-end acf forms
add_action('wp_print_styles', 'my_deregister_styles', 100);

function my_deregister_styles()
{
    //wp_deregister_style( 'acf' );
    //wp_deregister_style( 'acf-field-group' );
    //wp_deregister_style( 'acf-global' );
    //wp_deregister_style( 'acf-input' );
    //wp_deregister_style( 'acf-datepicker' );
}

function is_parent($post_ID = null)
{
    if ($post_ID === null) {
        global $post;
        $post_ID = $post->ID;
    }
    $query = new WP_Query(array('post_parent' => $post_ID, 'post_type' => 'any'));

    return $query->have_posts();
}

function acf_get_field_key($field_name, $post_id)
{
    global $wpdb;
    $acf_fields = $wpdb->get_results($wpdb->prepare("SELECT ID,post_parent,post_name FROM $wpdb->posts WHERE post_excerpt=%s AND post_type=%s", $field_name, 'acf-field'));
    // get all fields with that name.
    switch (count($acf_fields)) {
        case 0: // no such field
            return false;
        case 1: // just one result.
            return $acf_fields[0]->post_name;
    }
    // result is ambiguous
    // get IDs of all field groups for this post
    $field_groups_ids = array();
    $field_groups = acf_get_field_groups(array(
        'post_id' => $post_id,
    ));
    foreach ($field_groups as $field_group)
        $field_groups_ids[] = $field_group['ID'];

    // Check if field is part of one of the field groups
    // Return the first one.
    foreach ($acf_fields as $acf_field) {
        if (in_array($acf_field->post_parent, $field_groups_ids))
            return $acf_field->post_name;
    }
    return false;
}

add_action('acf/save_post', 'my_save_post');


function my_save_post($post_id)
{

    // bail early if not a contact_form post
    if (get_post_type($post_id) !== 'pedidos') {

        return;

    }


    // bail early if editing in admin
    if (is_admin()) {

        return;

    }


    // vars
    $post = get_post($post_id);


    // get custom fields (field group exists for content_form)
    $name = get_field('name', $post_id);
    $email = get_field('email', $post_id);


    // email data
    $to = 'contact@website.com';
    $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
    $subject = $post->post_title;
    $body = $post->post_content;


    // send email
    wp_mail($to, $subject, $body, $headers);

}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts()
{ ?>
    <style>
        /*@import "




        <?php echo get_template_directory_uri() ?>     /assets/css/main.css";*/
    </style>
<?php }

function get_breadcrumb($post_id)
{ ?>
    <?php if (!is_front_page()) { ?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page">
                <a href="<?php echo home_url() ?>" rel="nofollow">Home</a>
            </li>
            <?php if (is_single()) { ?>
                <li class="breadcrumb-item" aria-current="page">
                    <?php

                    $insurance = $post_id;
                    $terms = get_the_terms( $post_id, 'cobertura_tax' );

                    foreach ($terms as $term) {
                        $tax_page = get_field('tax_pagina', 'cobertura_tax_' . $term->term_id);
                    } ?>
                    <?php print_r(); ?>

                    <a href="<?php echo get_the_permalink($tax_page); ?>"><?php echo get_the_title($tax_page) ?></a>
                </li>
                <?php if (is_single()) { ?>
                    <li class="breadcrumb-item active" aria-current="page">
                        <?php the_title(); ?>
                    </li>
                <?php } ?>
            <?php } elseif (is_page()) { ?>
                <li class="breadcrumb-item active" aria-current="page">
                    <?php the_title(); ?>
                </li>
            <?php } elseif (is_search()) { ?>
                <li class="breadcrumb-item active" aria-current="page">
                    Resultados de búsqueda para...
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <?php echo the_search_query(); ?>
                </li>
                <?php
            }
            ?>
        </ol>
    </nav>
<?php } ?>
<?php } ?>
<?php
function has_heading()
{
    $post = get_post();
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] == 'acf/tax-heading') {
                return true;
            }
        }
    }
}

function has_price_variable($id)
{
    $post = get_post($id);
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] == 'acf/tax-heading') {
                $price = $block['attrs']['data']['heading-price'];
                echo $price;
            }

        }
    }
}

function get_price($post_id, $opt)
{
    $content_post = get_post($post_id);
    $content = $content_post->post_content;

    $blocks = parse_blocks($content);

    foreach ($blocks as $block) {
        if ($opt != '') {
            if ($block['blockName'] == 'acf/list-prices') {
                $price = $block['attrs']['data']['list-prices_' . $opt . '_list-price-price'];
                $name = $block['attrs']['data']['list-prices_' . $opt . '_list-price-name'];
                $text = $block['attrs']['data']['list-prices_' . $opt . '_list-price-text'];
                $data = array($price, $name, $text);
                return $data;
            }
        } else {
            if ($block['blockName'] == 'acf/tax-heading') {
                $price = $block['attrs']['data']['heading-price'];
                return $price;
            } else {
                return 'multiple';
            }
        }

    }
}

function get_blocks($post_id)
{
    $data[] = '';
    $content_post = get_post($post_id);
    $content = $content_post->post_content;
    $blocks = parse_blocks($content);

    foreach ($blocks as $block) {
        $data[] = $block;
    }
    return $data;
}

function get_cotization($post_id)
{
    $data[] = '';
    $content_post = get_post($post_id);
    $content = $content_post->post_content;
    $blocks = parse_blocks($content);

    foreach ($blocks as $block) {
        if ($block['blockName'] == 'acf/tax-heading') {
            if ($block['attrs']['data']['cta-buttons_0_cot_formula'] != "") {
                $formula = $block['attrs']['data']['cta-buttons_0_cot_formula'];
            } elseif ($block['attrs']['data']['cta-buttons_1_cot_formula'] != "") {
                $formula = $block['attrs']['data']['cta-buttons_1_cot_formula'];
            }
            return $formula;
        }
    }
}


add_action('wp_mail_failed', 'onMailError', 10, 1);
function onMailError($wp_error)
{
    echo "<pre>";
    print_r($wp_error);
    echo "</pre>";
}

function set_price_format($price)
{
    setlocale(LC_MONETARY, 'en_US');
    $price = money_format('%.0n', $price) . "\n";
    $price = $number = str_replace(',', '.', $price);
    return $price;
}

add_action('after_setup_theme', 'register_custom_nav_menus');
function register_custom_nav_menus()
{
    register_nav_menus(array(
        'primary' => 'Menu principal',
        'footer_menu' => 'Footer',
    ));
}

function prefix_category_title($title)
{
    if (is_tax()) {
        $title = single_cat_title('', false);
    }
    return $title;
}

add_filter('get_the_archive_title', 'prefix_category_title');

//Create the shortcode which will set the value for the DTX field
function cf7dtx_counter(){
    $val = get_option( CF7_COUNTER, 0) + 1;  //Increment the current count
    return $val;
}
add_shortcode('CF7_counter', 'cf7dtx_counter');

//Action performed when the mail is actually sent by CF7
function cf7dtx_increment_mail_counter(){
    $val = get_option( CF7_COUNTER, 0) + 1; //Increment the current count
    update_option(CF7_COUNTER, $val); //Update the settings with the new count
}
add_action('wpcf7_mail_sent', 'cf7dtx_increment_mail_counter');
