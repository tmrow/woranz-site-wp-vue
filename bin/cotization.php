<?php
require($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

// Begin of CORS things
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: X-Requested-With,Origin,Content-Type,Cookie,Accept');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
    die;
}
// End of CORS things

$requestBody = file_get_contents('php://input');
$requestBody = json_decode($requestBody, true);

if ($requestBody === null) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode([
        'errorMessage' => 'Please provide valid JSON',
    ]);
    die;
}

$cotization_formula = get_cotization($requestBody['insurance_id']);

if ($cotization_formula == 1) {

    foreach ($requestBody as $key => $value) {
        if ($key == 'contract_amount' and $value != "") {
            $monto = $value;
        } elseif ($key == 'sum_assured' and $value != "") {
            $suma = $value;
        } elseif ($key == 'equity' and $value != "") {
            $patrimonio = $value;
        }
    }

    $porcentaje = ($suma * 100) / $patrimonio;

    if ($porcentaje < 30) {
        $porcentaje = 30;
    }
    if ($porcentaje == 30) {
        $tasa = 0.005;
    } elseif ($porcentaje > 30 and $porcentaje <= 70) {
        $tasa = 0.0075;
    } elseif ($porcentaje > 70 and $porcentaje <= 100) {
        $tasa = 0.01;
    } elseif ($porcentaje > 100 and $porcentaje <= 150) {
        $tasa = 0.0125;
    } elseif ($porcentaje > 150 and $porcentaje <= 165) {
        $tasa = 0.015;
    } elseif ($porcentaje > 165 and $porcentaje <= 200) {
        $tasa = 0.02;
    } elseif ($porcentaje > 200 and $porcentaje <= 250) {
        $tasa = 0.025;
    } elseif ($porcentaje > 250 and $porcentaje <= 400) {
        $tasa = 0.03;
    } elseif ($porcentaje > 400 and $porcentaje <= 600) {
        $tasa = 0.035;
    } elseif ($porcentaje > 600 and $porcentaje <= 1000) {
        $tasa = 0.04;
    } else {
        $tasa = 0.04;
    }
    $total = ($suma * $tasa) / 4;

    if ($total < 300) {
        $response = "error";
    } else {
        $response = $total;
    }
}elseif ($cotization_formula==2){

    $capital = $requestBody['capital'];
    $socios = $requestBody['partners'];

    if ($capital > 50000) {
        $capital = 50000;
    }
    if ($capital <= 10000) {
        $montoFijo = 400.00;
    } else if ($capital > 10000 && $capital <= 20000) {
        $montoFijo = 720.00;
    } else if ($capital > 20000 && $capital <= 30000) {
        $montoFijo = 1050.00;
        } else if ($capital > 30000 && $capital <= 40000) {
        $montoFijo = 1350.00;
    } else if ($capital > 40000 && $capital <= 50000) {
        $montoFijo = 1600.00;
    }
    $response = $montoFijo;
}else{
    $response = "error";
}




// Json Header
header('Content-Type: application/json');

// return changed $requestBody
echo json_encode($response);
die;

