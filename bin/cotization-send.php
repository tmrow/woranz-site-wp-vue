<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

// Begin of CORS things
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: X-Requested-With,Origin,Content-Type,Cookie,Accept');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
    die;
}
// End of CORS things

$requestBody = file_get_contents('php://input');
$requestBody = json_decode($requestBody, true);
if ($requestBody === null) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode([
        'errorMessage' => 'Please provide valid JSON',
    ]);
    die;
}
$nombre = $requestBody["name"];
$vendedor = $requestBody["seller"];
$modo_de_contacto = $requestBody["contactMode"];
$cliente_email = $requestBody["contactData"];
$seller_email = $requestBody["seller_email"];
$price = $requestBody["price"];
$insurance = $requestBody["insurance"];

//USER
$body="";
if ($modo_de_contacto=="email" and $cliente_email!="" and $price>0) {
    $emailTo = $cliente_email;

    $subject = "Hola $nombre, te acerco tu cotización";
    $headers[] = 'From: '.$emailTo.' <'.$emailTo.'>';

    //$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/header.php');

    $body .= "<h2>Hola $nombre, soy $vendedor de Foms.</h2><p>Te comparto la cotización que realizaste en nuestro sitio y me pongo a disposición para avanzar con la emisión de tu seguro. </p><br><br>";

    if ($price) {
        $body .= "<p>TU COTIZACIÓN ESTIMADA</p>";
        $body .= "<h2 style='color:#29B89E;'><strong>".set_price_format($price)."</strong></h2>";
    }

    $body .="<hr><p><strong>Estos son los datos que ingresaste:</strong></p>";

    foreach ($requestBody as $key => $value) {
        if ($key=='capital' and $value!=""){
            $body .= "Capital : ".set_price_format($value);
            $body .= "<br>";
        }
        elseif ($key=='partners' and $value!=""){
            $body .= "Socios : $value";
            $body .= "<br>";
        }
        elseif ($key=='insurance' and $value!=""){
            $body .= "Tipo de seguro : $value";
            $body .= "<br>";
        }
        elseif ($key=='contract_amount' and $value!=""){
            $body .= "Monto del contrato :" .set_price_format($value);
            $body .= "<br>";
        }
        elseif ($key=='sum_assured' and $value!=""){
            $body .= "Suma asegurada : ".set_price_format($value);
            $body .= "<br>";
        }
    }
        // $body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/footer.php');

    $mailResult = wp_mail($emailTo, $subject, $body, $headers);
}


//ADMIN
if ($seller_email!=""){
    $emailTo =  $seller_email;
}else{
    $emailTo = 'comercial@foms.com.ar';
}

$subject = 'Cotización de póliza';
$headers[] = 'From: '.$emailTo.' <'.$emailTo.'>';

$body="";
//$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/header.php');


foreach ($requestBody as $key => $value) {
    if ($key=='name' and $value!=""){
        $body .= "Nombre : $value";
        $body .= "<br>";
    }
    elseif ($key=='surname' and $value!=""){
        $body .= "Apellido : $value";
        $body .= "<br>";
    }
    elseif ($key=='dni' and $value!=""){
        $body .= "DNI : $value";
        $body .= "<br>";
    }
    elseif ($key=='cuit' and $value!=""){
        $body .= "CUIT : $value";
        $body .= "<br>";
    }
    elseif ($key=='capital' and $value!=""){
        $body .= "Capital : ".set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='partners' and $value!=""){
        $body .= "Socios : $value";
        $body .= "<br>";
    }
   elseif ($key=='insurance' and $value!=""){
        $body .= "Seguro : $value";
       $body .= "<br>";
    }
    elseif ($key=='bussines_name' and $value!=""){
        $body .= "Razón social : $value";
        $body .= "<br>";
    }
    elseif ($key=='contract_amount' and $value!=""){
        $body .= "Monto del contrato :" .set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='sum_assured' and $value!=""){
        $body .= "Suma asegurada : ".set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='price' and $value!=""){
        $body .= "Valor de la cotización : " .set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='contactMode' and $value!=""){
        if ($value=='phone'){
            $value='teléfono';
        }
        $body .= "Modo de contacto : $value";
    }
    elseif ($key=='contactData' and $value!=""){
        $body .= " - $value";
        $body .= "<br>";
    }
    elseif ($key=='error' and $value!=""){
        $body .= "Error : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller' and $value!=""){
        $body .= "Vendedor : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller_email' and $value!=""){
        $seller_email = $value;
    }
}
//$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/footer.php');

$mailResult = wp_mail($emailTo, $subject, $body, $headers);

// Json Header
header('Content-Type: application/json');


// return changed $requestBody
echo json_encode($mailResult); die;
