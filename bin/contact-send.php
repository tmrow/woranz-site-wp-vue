<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

// Begin of CORS things
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: X-Requested-With,Origin,Content-Type,Cookie,Accept');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
    die;
}
// End of CORS things

$requestBody = file_get_contents('php://input');
$requestBody = json_decode($requestBody, true);
if ($requestBody === null) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode([
        'errorMessage' => 'Please provide valid JSON',
    ]);
    die;
}

$nombre = $requestBody["name"];
$vendedor = $requestBody["seller"];
$modo_de_contacto = $requestBody["contactMode"];
$cliente_email = $requestBody["contactData"];
$seller_email = $requestBody["seller_email"];
$insurance = $requestBody['insurance'];


$subject = 'Consulta de '.$nombre.' por  '.$insurance;
$headers[] = 'From: '.$emailTo.' <'.$emailTo.'>';

if ($cliente_email!=""){
    $headers[] = 'Cco:'.$cliente_email;
}

$body="";
if ($cliente_email!="") {
    $emailTo = $cliente_email;

    //$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/header.php');

    $body .= "<p>Hola $nombre, soy $vendedor de Foms.</p><p>Te escribo para avisarte que recibí tu pedido y que me voy a estar contactando por este medio lo antes posible.</p><br><br>";

    $body .= "<p>Saludos <br>$vendedor<br>";

    //$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/footer.php');


    $mailResult = wp_mail($emailTo, $subject, $body, $headers);
}



$body="";
$body .= "<strong>Contacto desde el sitio</strong><br><br>";
foreach ($requestBody as $key => $value) {
    if ($key=='name' and $value!=""){
        $body .= "<b>Nombre</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='surname' and $value!=""){
        $body .= "<b>Apellido</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='dni' and $value!=""){
        $body .= "<b>DNI</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='cuit' and $value!=""){
        $body .= "<b>CUIT</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='capital' and $value!=""){
        $body .= "<b>Capital</b> : ".set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='partners' and $value!=""){
        $body .= "<b>Socios</b> : $value";
        $body .= "<br>";
    }
   elseif ($key=='insurance' and $value!=""){
        $body .= "<b>Seguro</b> : $value";
       $body .= "<br>";
    }
    elseif ($key=='insurance_id' and $value!=""){
        if ($requestBody['insurance']==""){
            $body .= "<b>Seguro</b> : ".get_the_title($value)."";
            $body .= "<br>";
        }
    }
    elseif ($key=='bussines_name' and $value!=""){
        $body .= "<b>Razón social</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='contract_amount' and $value!=""){
        $body .= "<b>Monto del contrato</b> :" .set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='sum_assured' and $value!=""){
        $body .= "<b>Suma asegurada</b> : ".set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='price' and $value!=""){
        $body .= "<b>Valor de la cotización</b> : " .set_price_format($value)."";
        $body .= "<br>";
    }
    elseif ($key=='comments' and $value!=""){
        $body .= "<b>Comentarios</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='contactMode' and $value!=""){
        $body .= "<b>Modo de contacto</b> : $value";
    }
    elseif ($key=='contactData' and $value!=""){
        $body .= " - $value";
        $body .= "<br>";
    }
    elseif ($key=='error' and $value!=""){
        $body .= "<b>Error</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller' and $value!=""){
        $body .= "<b>Vendedor</b> : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller_email' and $value!=""){
        $seller_email = $value;
    }
}

$body.= "<p>Saludos<br>$vendedor<br>";
//$body .= file_get_contents(TEMPLATEPATH . 'template-parts/emails/footer.php');

if ($seller_email!=""){
    $emailTo =  $seller_email;
}else{
    $emailTo = 'comercial@foms.com.ar';
}

$mailResult = wp_mail($emailTo, $subject, $body, $headers);


// Json Header
header('Content-Type: application/json');


// return changed $requestBody
echo json_encode($requestBody); die;
