<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

// Begin of CORS things
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: X-Requested-With,Origin,Content-Type,Cookie,Accept');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
    die;
}
// End of CORS things

$requestBody = file_get_contents('php://input');
$requestBody = json_decode($requestBody, true);
if ($requestBody === null) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode([
        'errorMessage' => 'Please provide valid JSON',
    ]);
    die;
}

$body="";
$insurance = $requestBody['insurance'];

foreach ($requestBody as $key => $value) {
    if ($key=='name' and $value!=""){
        $body .= "Nombre : $value";
        $body .= "<br>";
    }
    elseif ($key=='surname' and $value!=""){
        $body .= "Apellido : $value";
        $body .= "<br>";
    }
    elseif ($key=='dni' and $value!=""){
        $body .= "DNI : $value";
        $body .= "<br>";
    }
    elseif ($key=='cuit' and $value!=""){
        $body .= "CUIT : $value";
        $body .= "<br>";
    }
   elseif ($key=='insurance' and $value!=""){
        $body .= "Seguro : $value";
       $body .= "<br>";
    }
    elseif ($key=='payment' and $value!=""){
        $body .= "Metodo de pago : $value";
        $body .= "<br>";
    }
    elseif ($key=='price' and $value!=""){
        $body .= "Pago : $ $value";
        $subject = 'Pago poliza';
        $body .= "<br>";
    }
    elseif ($key=='contactMode' and $value!=""){
        if ($value=='phone'){
            $value='teléfono';
        }
        $body .= "Modo de contacto : $value";
    }
    elseif ($key=='contactData' and $value!=""){
        $body .= " - $value";
        $body .= "<br>";
    }
    elseif ($key=='error' and $value!=""){
        $body .= "Error : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller' and $value!=""){
        $body .= "Vendedor : $value";
        $body .= "<br>";
    }
    elseif ($key=='seller_email' and $value!=""){
        $seller_email = $value;
    }
}
if ($seller_email!=""){
    $emailTo =  $seller_email;
}else{
    $emailTo = 'comercial@foms.com.ar';
}

$subject = 'Consulta por poliza - '. $insurance;
$headers[] = 'From: '.$emailTo.' <'.$emailTo.'>';

$mailResult = wp_mail($emailTo, $subject, $body, $headers);

function tipo_de_contenido_html() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'tipo_de_contenido_html' );

// Json Header
header('Content-Type: application/json');


// return changed $requestBody
echo json_encode($mailResult); die;
