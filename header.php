<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <?php if(is_page('alquileres') OR is_page('universitaria') OR is_page('comercial')) {?>
    		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/landing/assets/css/landing.css">
    		<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700|Roboto&display=swap" rel="stylesheet">
    		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/landing/assets/css/swiper.min.css">
    	<?php } else { ?>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/main.css">
        <?php } ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- nav menu -->
<?php if(!is_page('alquileres') AND !is_page('universitaria') AND !is_page('comercial')) {?>
<header>
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand d-none d-lg-block" href="<?php echo home_url() ?>">
                <!--<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-header.svg" alt="">-->
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/foms-logo.svg" alt="" width="130">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="offcanvas" data-target="#offcanvas" data-canvas="body">
                <i class="fas fa-bars"></i>
            </button>

            <nav id="offcanvas" class="navmenu navmenu-default navmenu-fixed-left offcanvas pt-4" role="navigation">

                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="offcanvas" data-target="#coberturas" data-canvas="body">Coberturas</a>
                    </li>
                </ul>

                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => 'nav flex-column',
                        'fallback_cb' => '',
                        'menu_id' => 'offcanvas-menu',
                        'depth' => 2,
                        'walker' => new Understrap_WP_Bootstrap_Navwalker(),
                    )
                ); ?>
            </nav>
            <nav id="coberturas" class="navmenu navmenu-default navmenu-fixed-left offcanvas pt-4" role="navigation">
                <?php
                $terms = get_terms('cobertura_tax', array(
                    'hide_empty' => false,
                ));
                foreach ($terms as $term) { ?>
                        <a href="<?php echo get_the_permalink(get_field('tax_pagina', 'cobertura_tax_' . $term->term_id)); ?>" class="nav-link w-100">
                            <img class=" btn-icon"
                                 src="<?php the_field('tax_icon', 'cobertura_tax_' . $term->term_id); ?>"
                                 width="40">
                            <span><?php echo $term->name; ?></span>
                        </a>

                <?php } ?>
            </nav>


            <a class="navbar-brand d-block d-lg-none" href="<?php echo home_url(); ?>">
                <!--<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-dark.svg" alt="">-->
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/foms-logo.svg" alt="" width="100">

            </a>

            <a class="nav-link anchor d-block d-lg-none" href="tel:+541160095100">
                <i class="fas fa-phone"></i>
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-md-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link anchor" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Coberturas <i class="fas fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="row">
                                <?php
                                $terms = get_terms('cobertura_tax', array(
                                    'hide_empty' => false,
                                ));
                                foreach ($terms as $term) { ?>
                                    <div class="col-3 align-self-center btn-header">
                                        <a href="<?php echo get_the_permalink(get_field('tax_pagina', 'cobertura_tax_' . $term->term_id)); ?>">
                                            <img class="float-left btn-icon"
                                                 src="<?php the_field('tax_icon', 'cobertura_tax_' . $term->term_id); ?>"
                                                 width="60">
                                            <h5><?php echo $term->name; ?></h5>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                </ul>
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => 'navbar-nav ml-auto',
                        'fallback_cb' => '',
                        'menu_id' => 'main-menu',
                        'depth' => 2,
                        'walker' => new Understrap_WP_Bootstrap_Navwalker(),
                    )
                ); ?>
            </div>
        </div>
    </nav>
</header>

<!-- End nav menu -->
       <?php } ?>
