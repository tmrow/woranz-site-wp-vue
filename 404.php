<?php get_header(); ?>
<section class="block bg-yellow has-image" id="heading">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center">

                    <h1 class="entry-title">
                        Lo sentimos, no encontramos la página que estás buscando
                    </h1>


                    <p class="lead">
                        Intentá nuevamente
                    </p>

            </div>

                <div class="col-lg-6 text-right align-self-end buttons">

                </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
