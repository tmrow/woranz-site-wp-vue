<?php
/**
 * Block Name: Diferent
 *
 */
?>
<section id="diferent" class="block bg-yellow">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2>Somos diferentes</h2>
            </div>
            <div class="col-10 col-md-4">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/personas.svg" alt="">
                <h5>Personas trabajando con personas</h5>
                <p class="lead">Sos importante para nosotros, tu atención será personalizada siempre.</p>
            </div>

            <div class="col-10 col-md-4">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/simple.svg" alt="">
                <h5>Ayudarte a avanzar</h5>
                <p class="lead">Queremos que tu experiencia sea ágil, accesible y online.</p>
            </div>
            <div class="col-10 col-md-4">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/letra-chica.svg" alt="">
                <h5>Sin letra chica</h5>
                <p class="lead">Nuestras condiciones siempre a la vista y a disposición.</p>
            </div>
        </div>
    </div>

</section>
