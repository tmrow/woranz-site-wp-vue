<?php
/**
 * Block Name: List prices
 *
 */
?>
<section class="block" id="products">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <?php if (get_field('list-price-section-title')) { ?>
                    <h2 class="mb-0"><?php the_field('list-price-section-title'); ?></h2>
                <?php } ?>
                <?php if (get_field('list-price-section-text')) { ?>
                    <p class="lead mb-4">
                        <?php the_field('list-price-section-text'); ?>
                    </p>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <?php $i=0; ?>
            <?php
            $count = count(get_field('list-prices'));
            $divide = 12 / $count;
            if ($count % 2 == 0) {
                $cols = 'col-md-6 col-lg-' . $divide;
            } else {
                $cols = 'col-lg-' . $divide;
            }
            ?>
            <?php while (have_rows('list-prices')) : the_row(); ?>
                <div class="<?php echo $cols ?>">
                    <?php set_query_var('option', $i); ?>
                    <?php get_template_part('template-parts/components/product-card-price'); ?>
                </div>
                <?php
            $i++;
            endwhile;
            ?>
        </div>
    </div>
</section>
