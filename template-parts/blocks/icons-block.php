<?php
/**
 * Block Name: Icons block
 *
 */
?>
    <section id="icons-block" class="block <?php if (get_field('icons-bg')) {
        echo 'bg-yellow';
    } ?>">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7 text-center">
                    <?php if (get_field('icons-title')) { ?>
                        <h2 <?php if (get_field('icons-text')) {
                            echo 'mb-0';
                        } ?>><?php the_field('icons-title') ?></h2>
                    <?php } ?>
                    <?php if (get_field('icons-text')) { ?>
                        <p class="lead"><?php the_field('icons-text') ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="row icons">
                <?php if (get_field('icons-col')) {
                    $count = count(get_field('icons-col'));
                    $divide = 12 / $count;
                    if ($count % 2 == 0) {
                        $cols = 'col-md-6 col-lg-' . $divide . ' ' .get_field('icons_union');
                    } else {
                        $cols = 'col-lg-' . $divide . ' ' .get_field('icons_union');
                    }
                    $i = 1;
                    ?>
                    <?php while (have_rows('icons-col')) : the_row(); ?>
                        <div class="<?php echo $cols; ?> text-center icon-block">
                            <?php if (get_sub_field('icons-col-icon')) { ?>
                                <div class="icon text-center">
                                    <img src="<?php the_sub_field('icons-col-icon'); ?>" width="150" height="150">
                                </div>
                            <?php } ?>
                            <h5><?php the_sub_field('icons-col-title'); ?></h5>
                            <p class="lead">
                                <?php the_sub_field('icons-col-text'); ?>
                            </p>
                        </div>
                    <?php endwhile; ?>
                <?php } ?>
            </div>
        </div>
    </section>
<?php
