<form>
    <!-- STEP 1 -->
    <transition name="fade">
        <div class="form-block" v-if="step === 1">
            <div class="seller-text">
                <h1>¡Hola! soy <?php echo $seller_name; ?></h1>
                <p class="lead">
                    <big>
                        <?php if ($action == 'cotizar') {
                            echo 'Voy a ayudarte a cotizar tu poliza';
                        } elseif ($action == 'pedir') {
                            echo 'Voy a ayudarte para que tengas tu seguro lo más rápido posible';
                        } else {
                            echo 'Estoy para responder todas tus preguntas';
                        } ?>
                    </big>
                </p>
            </div>
            <div class="row mt-4">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Decime tu nombre</label>
                        <input type="text"
                               v-model="form.name"
                               v-validate="'required'"
                               id="name"
                               name="name"
                               class="form-control"
                               data-vv-name="nombre"
                               :class="{ 'is-invalid': errors.has('nombre') }"/>
                        <div v-if="errors.has('nombre')" class="invalid-feedback">
                            {{ errors.first('nombre') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Y tu apellido</label>
                        <input type="text"
                               v-model="form.surname"
                               v-validate="'required'"
                               id="surname"
                               name="surname"
                               class="form-control"
                               data-vv-name="apellido"
                               :class="{ 'is-invalid': errors.has('apellido') }"/>
                        <div v-if="errors.has('apellido')" class="invalid-feedback">
                            {{ errors.first('apellido') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center form-block-actions">
                <button class="btn btn-primary btn-lg px-4"
                        @click.stop.prevent="handleSubmit()">
                    SIGUIENTE
                </button>
            </div>
        </div>
    </transition>


    <!-- STEP 2 -->
    <transition name="fade">
        <?php if ($insurance == '2') { ?>
            <!-- CAUCIONES DE ALQUILER -->
            <div class="form-block" v-if="step === 2">
                <div class="seller-text">
                    <h1>Encantada {{form.name}}</h1>
                    <p class="lead">
                        <big>
                            Te pido algunos datos para poder emitir tu poliza
                        </big>
                    </p>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dni">DNI</label>
                            <input type="tel"
                                   v-model="form.dni"
                                   v-validate="'required'"
                                   id="dni"
                                   name="dni"
                                   class="form-control"
                                   data-vv-name="DNI"
                                   :class="{ 'is-invalid': errors.has('DNI') }"/>
                            <div v-if="errors.has('DNI')" class="invalid-feedback">
                                {{ errors.first('DNI') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center form-block-actions">
                    <button class="btn btn-primary btn-lg px-4"
                            @click.stop.prevent="handleSubmit()">
                        SIGUIENTE
                    </button>
                </div>
            </div>
        <?php } ?>
    </transition>

    <!-- CONTACTO -->
    <transition name="fade">
        <div class="form-block" v-if="step === 3">
            <div class="seller-text">
                <h1>¿Como preferís que te contactemos cuando esté lista tu poliza?</h1>
            </div>
            <div class="row mb-2 justify-content-center">
                <div class="col-md-6">
                    <div class="customradio">
                        <div class="customradio-primary">
                            <input type="radio" name="radio" id="radio1" value="whatsapp"
                                   v-model="form.contact" checked/>
                            <label for="radio1">Whatsapp</label>
                            <input type="text"
                                   name="contact-whatsapp"
                                   class="form-control"
                                   v-if="form.contact=='whatsapp'"
                                   v-model="form.whatsapp"
                                   placeholder="Ingresá el número"
                                   v-validate="'required'"
                                   data-vv-name="número de Whatsapp"
                                   :class="{ 'is-invalid': errors.has('número de Whatsapp') }"
                            >
                            <div v-if="errors.has('número de Whatsapp')"
                                 class="invalid-feedback">
                                {{ errors.first('número de Whatsapp') }}
                            </div>

                        </div>
                        <div class="customradio-primary">
                            <input type="radio" name="radio" id="radio2" value="telefono"
                                   v-model="form.contact"/>
                            <label for="radio2">Teléfono</label>
                            <input type="text"
                                   name="contact-phone"
                                   class="form-control"
                                   v-if="form.contact=='telefono'"
                                   v-model="form.phone"
                                   placeholder="Ingresá el número"
                                   v-validate="'required'"
                                   data-vv-name="número de teléfono"
                                   :class="{ 'is-invalid': errors.has('número de teléfono') }"
                            >
                            <div v-if="errors.has('número de teléfono')"
                                 class="invalid-feedback">
                                {{ errors.first('número de teléfono') }}
                            </div>
                        </div>
                        <div class="customradio-primary">
                            <input type="radio" name="radio" id="radio3" value="email"
                                   v-model="form.contact"/>
                            <label for="radio3">Email</label>
                            <input type="email"
                                   name="contact-email"
                                   class="form-control"
                                   v-if="form.contact=='email'"
                                   v-model="form.email"
                                   placeholder="Ingresá tu email"
                                   v-validate="'required|email'"
                                   data-vv-name="email"
                                   :class="{ 'is-invalid': errors.has('email') }"
                            >
                            <div v-if="errors.has('email')" class="invalid-feedback">
                                {{ errors.first('email') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center form-block-actions">
                <button class="btn btn-primary btn-lg px-4"
                        @click.stop.prevent="handleSubmit()">
                    SIGUIENTE
                </button>
            </div>
        </div>
    </transition>

    <!-- PAGO -->
    <transition name="fade">
        <div class="form-block" v-if="step === 4">
            <div class="seller-text">
                <h1>¿Como preferís pagar tu poliza?</h1>
            </div>
            <div class="row mb-2 justify-content-center">
                <div class="col-md-6">
                    <div class="customradio">
                        <!--<div class="customradio-primary">
                            <input type="radio" name="radio" id="radio1" value="mercadopago"
                                   v-model="form.payment" />
                            <label for="radio1">Pago online con MercadoPago</label>
                        </div>-->
                        <div class="customradio-primary">
                            <input type="radio" name="radio" id="radio2" value="oficinas"
                                   v-model="form.payment"/>
                            <label for="radio2">Pagar en nuestras oficinas</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center form-block-actions">
                <button class="btn btn-primary btn-lg px-4"
                        @click.stop.prevent="handleSubmit()">
                    SIGUIENTE
                </button>
            </div>
        </div>
    </transition>

    <!-- PAGO -->
    <transition name="fade">
        <div class="form-block" v-if="step === 5">
            <div class="seller-text">
                <h1>Genial {{form.name}}</h1>
                <p class="lead">Recibimos tu pedido. Te esperamos en nuestras oficinas de Rivadavia 611. Piso 5.
                    para realizar el pago.</p>
            </div>
            <div class="text-center form-block-actions">
                <a href="<?php echo home_url(); ?>" class="btn btn-primary btn-lg px-4">
                    LISTO
                </a>
            </div>
        </div>
    </transition>

</form>
