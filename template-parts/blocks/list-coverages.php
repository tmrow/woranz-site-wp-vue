<?php
/**
 * Block Name: List coverages
 *
 */
?>
<section class="block" id="coverages" v-if="j=1">
    <div class="container min-gutters">
        <div class="row">
            <div class="col-12 text-center">
                <?php if (get_field('coverage-title')) { ?>
                    <h2 class="mb-0"><?php the_field('coverage-title'); ?></h2>
                <?php } else { ?>
                    <h2 class="mb-0">Nuestras coberturas</h2>
                <?php } ?>
                <?php if (get_field('coverage-text')) { ?>
                    <p class="lead">
                        <?php the_field('coverage-text') ?>
                    </p>
                <?php } ?>
            </div>
        </div>
        <div class="row mt-3">
            <?php
            $terms = get_terms('cobertura_tax', array(
                'hide_empty' => false,
            ));
            foreach ($terms as $term) { ?>
                <div class="col-lg-6 col-xl-4">
                    <?php
                    set_query_var('term', $term);
                    get_template_part('template-parts/components/button-coverage');
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<script>

</script>
