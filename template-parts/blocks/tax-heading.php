<?php
/**
 * Block Name: Tax heading
 *
 */
$title = get_the_title();
?>
<section class="block bg-yellow <?php if (get_field('heading-image') and !get_field('heading-form')) {
    echo 'has-image';
} ?>" id="heading">
    <div class="container">
        <div class="row">
            <?php get_breadcrumb(get_the_ID()); ?>
            <div class="col-lg-6 align-self-center">
                <?php if (get_field('heading-head')) { ?>
                    <h6><?php the_field('heading-head'); ?></h6>
                <?php } ?>
                <?php if (get_field('heading-title')) { ?>
                    <h1 class="entry-title">
                        <?php the_field('heading-title'); ?>
                    </h1>
                <?php } else { ?>
                    <h1 class="entry-title">
                        <?php the_title(); ?>
                    </h1>
                <?php } ?>
                <?php if (get_field('heading-price')) { ?>
                    <h2 class="display">
                        <?php echo set_price_format(get_field('heading-price')); ?>
                    </h2>
                <?php } ?>
                <?php if (get_field('heading-description')) { ?>
                    <p class="lead">
                        <?php the_field('heading-description'); ?>
                    </p>
                <?php } ?>
                <?php if (get_field('heading-image')) { ?>
                    <?php if (get_field('cta-buttons')) {
                        get_template_part('template-parts/components/cta-buttons');
                    } ?>
                <?php } ?>
            </div>
            <?php if (!get_field('heading-image') and !get_field('heading-form')) { ?>
                <div class="col-lg-6 text-right align-self-end buttons">
                    <?php if (get_field('cta-buttons')) {
                        get_template_part('template-parts/components/cta-buttons');
                    } ?>
                </div>
            <?php } ?>
            <?php if (get_field('heading-image') and !get_field('heading-form')) { ?>
                <div class="col-lg-6">
                    <div class="break-container right">
                        <?php

                        $image = get_field('heading-image');

                        // vars
                        $url = $image['url'];
                        $title = $image['title'];
                        $alt = $image['alt'];
                        $caption = $image['caption'];

                        // thumbnail
                        $size = 'large';
                        $thumb = $image['sizes'][$size];
                        $width = $image['sizes'][$size . '-width'];
                        $height = $image['sizes'][$size . '-height'];
                        ?>

                        <div class="heading-image" style="background-image: url('<?php echo $url; ?>')">

                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (get_field('heading-form')) { ?>
                <div class="col-lg-6">
                    <div class="card form">
                        <div class="card-body pt-3">
                           <!--<h3 class="mt-1 mb-0">Completá el formulario</h3>
                            <p class="lead mb-3 mt-3">Y unos de nuestros asesores se comunicará con vos</p>-->
                            <?php echo do_shortcode(the_field('heading-form')) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>

</section>
<?php if (get_field('heading-image') and get_field('heading-form')) { ?>
<div class="break-container" style="margin-top: -90px; z-index: 0;">
    <?php

    $image = get_field('heading-image');

    // vars
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];

    // thumbnail
    $size = 'large';
    $thumb = $image['sizes'][$size];
    $width = $image['sizes'][$size . '-width'];
    $height = $image['sizes'][$size . '-height'];
    ?>

    <img src="<?php echo $url; ?>" class="img-fluid w-100" style="display: block; position: relative">
</div>
<?php } ?>
<?php if (get_field('heading-image') and !get_field('heading-form')) { ?>
<div class="break-container" style=" z-index: 0;">
    <img src="<?php echo $url; ?>" class="img-fluid w-100 d-block d-lg-none" style="display: block; position: relative">
</div>
<?php } ?>
