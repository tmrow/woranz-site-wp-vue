<?php
/**
 * Block Name: List insurances
 *
 */
$coberturas = get_field('cat_insurance');
//print_r(get_field('cta-buttons', 'block_5d93195b412ee'));

$args =
    array(
        'posts_per_page' => -1,
        'limit' => -1,
        'post_type' => 'cobertura',
        'tax_query' => array(
            array(
                'taxonomy' => 'cobertura_tax',
                'field' => 'term_id',
                'terms' => $coberturas,
            )
        )
    );

$the_query = new WP_Query($args); ?>
<section class="block" id="products">
    <div class="container">
        <div class="row justify-content-center">
            <?php
            while ($the_query->have_posts()) {
                $the_query->the_post(); ?>
                <div class="col-12 col-lg-6 col-xl-4">
                    <?php get_template_part('template-parts/components/product-card'); ?>
                </div>
                <?php
            }
            ?>
            <div class="col caller text-center align-self-start px-2">
                <div class="inner">
                    <h3>¿Ninguna se adapta a lo que buscás?</h3>
                    <p class="lead mb-0">¡Hablemos! podemos personalizar tu poliza de acuerdo a lo que necesites.</p>
                </div>
            </div>
        </div>
    </div>
</section>
