<?php
/**
 * Block Name: Help
 *
 */
?>
<?php
if (get_post_type($post_id) == 'page') {
    $terms = get_terms('cobertura_tax', array(
        'hide_empty' => false,
    ));
    foreach ($terms as $term) {
        $tax_page = get_field('tax_pagina', 'cobertura_tax_' . $term->term_id);
        if ($tax_page == $post_id) {
            $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
        }
    }
}
if (get_post_type($post_id) == 'cobertura') {
    $terms = wp_get_post_terms($post_id, 'cobertura_tax');
    foreach ($terms as $term) {
        $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
    }
}
if (empty($sellers)) {
    $args = array(
        'numberposts' => 1,
        'post_type' => 'vendedor'
    );
    $sellers = get_posts($args);
}
foreach ($sellers as $seller) {
    $seller_name = $seller->post_title;
    $seller_image = get_the_post_thumbnail($seller->ID);
    $seller_image_url = get_the_post_thumbnail_url($seller->ID);
    $seller_gender = get_field('seller_gender', $seller->ID);
    $seller_email = get_field('seller_email', $seller->ID);
}
?>
<section class="block <?php if(get_field('help-bg')){echo 'bg-yellow';} ?>" id="help">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-6 col-lg-3">
                <?php if (get_field('help-image')) {
                    $image = get_field('help-image');
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][ $size ];
                    ?>
                    <div class="help-image bg-cover" style="background-image: url(<?php echo $thumb; ?>)"></div>
                <?php } else { ?>
                    <div class="help-image bg-cover" style="background-image: url('<?php echo $seller_image_url ?>')"></div>
                <?php } ?>
            </div>
            <div class="col-lg-6">
                <div class="text-center text-lg-left">
                    <?php if (get_field('help-title')) { ?>
                        <h2><?php the_field('help-title'); ?></h2>
                    <?php } else { ?>
                        <h2>¿Necesitás ayuda?</h2>
                    <?php } ?>
                    <?php if (get_field('help-text')) { ?>
                        <p class="lead"><?php the_field('help-text'); ?></p>
                    <?php } else { ?>
                        <p class="lead">Comunicate con nosotros y charlá con uno de nuestros asesores</p>
                    <?php } ?>
                    <?php if (get_field('cta-buttons')) {
                        get_template_part('template-parts/components/cta-buttons');
                    } ?>
                </div>
            </div>
        </div>
    </div>
</section>
