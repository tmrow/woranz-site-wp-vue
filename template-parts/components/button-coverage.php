<a href="<?php echo get_the_permalink(get_field('tax_pagina', 'cobertura_tax_' . $term->term_id)); ?>" class="btn-coverage">
    <img class="float-left btn-icon" src="<?php the_field('tax_icon', 'cobertura_tax_' . $term->term_id); ?>">
    <p class="lead"><?php echo $term->name; ?></p>
    <img class="b-arrow" src="<?php echo get_template_directory_uri() ?>/assets/img/SECTION/arrow.svg" width="12" height="33"
         alt="Ícono de dirección">
</a>
