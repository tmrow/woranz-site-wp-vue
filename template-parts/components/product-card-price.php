<div class="card price">
    <?php if (get_sub_field('list-price-icon')) { ?>
        <div class="icon">
            <img src="<?php echo get_sub_field('list-price-icon'); ?>"
                 alt="<?php echo get_sub_field('list-price-name'); ?>">
        </div>
    <?php } ?>
    <div class="card-body">
        <?php if (get_sub_field('list-price-name')) { ?>
            <p class="card-head">
                <strong>
                    <?php the_sub_field('list-price-name'); ?>
                </strong>
            </p>
        <?php } ?>
        <?php if (get_sub_field('list-price-price')) { ?>
            <h1 class="card-price">
                <?php echo set_price_format(get_sub_field('list-price-price')); ?>
            </h1>
        <?php } else { ?>
            <h2 class="card-price">
                Consultar
            </h2>
        <?php } ?>
        <?php if (get_sub_field('list-price-text')) { ?>
            <p class="mb-3">
                <?php the_sub_field('list-price-text'); ?>
            </p>
        <?php } ?>
        <div class="card-actions">
            <?php if (get_sub_field('list-pay')) { ?>
                <!--  acá hay que ver cuales se piden online y cuales se cotizan -->
                <a href="<?php echo home_url(); ?>/pago?id=<?php echo get_the_ID(); ?>&opt=<?php echo $option; ?>"
                   class="btn btn-primary btn-sm btn-block">PEDIR AHORA</a>
            <?php } else { ?>
                <a href="<?php echo home_url(); ?>/contacto?id=<?php echo get_the_ID(); ?>"
                   class="btn btn-primary btn-sm btn-block">CONSULTAR</a>
            <?php } ?>
        </div>
    </div>
</div>
