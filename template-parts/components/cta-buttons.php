<div class="btn-group">
    <?php while (have_rows('cta-buttons')) : the_row();
        if (get_sub_field('button_select') == 'pedir') { ?>
            <a href="<?php echo home_url(); ?>/pago?id=<?php echo get_the_ID(); ?>"
               class="btn btn-primary btn-xs-block">
                PEDIR ONLINE
            </a>
        <?php } elseif (get_sub_field('button_select') == 'cotizar') { ?>
            <a href="<?php echo home_url(); ?>/cotizacion?id=<?php echo get_the_ID(); ?>"
               class="btn btn-primary btn-xs-block">
                COTIZAR ONLINE
            </a>
        <?php } elseif (get_sub_field('button_select') == 'consultar') { ?>
            <a href="<?php echo home_url(); ?>/contacto?id=<?php echo get_the_ID(); ?>"
               class="btn btn-primary btn-xs-block">
                CONSULTAR ONLINE
            </a>
        <?php } elseif (get_sub_field('button_select') == 'whatsapp') { ?>
            <?php if (is_single()) { ?>
                <a href="
                    https://api.whatsapp.com/send?phone=5491167572460&text=Hola%20Martina,%20estoy%20viendo%20<?php the_title(); ?>,%20%C2%BFpodr%C3%ADas%20ayudarme?"
                   class="btn btn-info btn-xs-block">
                    PEDIR POR WHATSAPP
                </a>
            <?php } else { ?>
                <a href="
                    https://api.whatsapp.com/send?phone=5491167572460&text=Hola%20Martina,%20estoy%20viendo%20<?php the_title(); ?>,%20%C2%BFpodr%C3%ADas%20ayudarme?"
                   class="btn btn-info btn-xs-block">
                    CONSULTAR POR WHATSAPP
                </a>
            <?php } ?>
        <?php } elseif (get_sub_field('button_select') == 'llamada') { ?>
            <a href="<?php echo home_url(); ?>//contacto?id=<?php echo get_the_ID(); ?>&action='call'" class="btn btn-info btn-xs-block">
                QUIERO QUE ME LLAMEN
            </a>
        <?php } elseif (get_sub_field('button_select') == 'custom') { ?>
            <a href="<?php the_sub_field('button_link'); ?>"
               class="btn <?php the_sub_field('button_color'); ?> btn-xs-block">
                <?php the_sub_field('button_text'); ?>
            </a>
        <?php } ?>
    <?php endwhile; ?>
</div>
