<transition name="fade">
    <!-- CAUCIONES DE ALQUILER -->
    <div class="form-block" v-if="step === 2">
        <div class="seller-text">
            <h1>Encantada {{form.name}}</h1>
            <p class="lead">
                <big>
                    Te pido algunos datos para poder emitir tu poliza
                </big>
            </p>
        </div>
        <div class="row mt-4 justify-content-center">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="dni">DNI</label>
                    <input type="tel"
                           v-model="form.dni"
                           v-validate="'required'"
                           id="dni"
                           name="dni"
                           class="form-control"
                           data-vv-name="DNI"
                           :class="{ 'is-invalid': errors.has('DNI') }"/>
                    <div v-if="errors.has('DNI')" class="invalid-feedback">
                        {{ errors.first('DNI') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center form-block-actions">
            <button class="btn btn-primary btn-lg px-4"
                    @click.stop.prevent="handleSubmit()">
                SIGUIENTE
            </button>
        </div>
    </div>
</transition>
