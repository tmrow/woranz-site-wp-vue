<div class="card product">
    <?php if (has_post_thumbnail()) { ?>
        <div class="card-img" style="background-image: url('<?php the_post_thumbnail_url() ?>')"></div>
    <?php } ?>
    <div class="card-body">
        <h5 class="card-title">
            <?php if (get_field('hide_landing', get_the_ID()) != 1) { ?>
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            <?php } else {
                the_title();
            } ?>
        </h5>
        <?php if (get_price(get_the_id(), '') != "") { ?>
            <h5><?php echo set_price_format(get_price(get_the_id(), '')); ?> </h5>
        <?php } ?>
        <div class="card-actions">
            <!--  acá hay que ver cuales se piden online y cuales se cotizan -->
            <?php if (get_field('hide_landing', get_the_ID()) != 1) { ?>
                <a href="<?php the_permalink(); ?>"
                   class="btn btn-primary btn-sm float-left">MÁS INFORMACIÓN</a>
            <?php } else { ?>
                <?php if (get_price(get_the_id(), '') != "") { ?>
                    <?php if (get_price(get_the_id(), '') == "multiple") { ?>
                        <a href="<?php the_permalink(); ?>#products" class="btn btn-primary btn-sm float-left">VER
                            OPCIONES</a>

                    <?php } else { ?>
                        <a href="<?php echo home_url() ?>/pago?id=<?php echo get_the_ID(); ?>"
                           class="btn btn-primary btn-sm float-left">PEDIR AHORA</a>
                    <?php } ?>

                <?php } elseif (get_cotization(get_the_id()) != "") { ?>
                    <a href="<?php echo home_url() ?>/cotizacion?id=<?php echo get_the_ID(); ?>"
                       class="btn btn-primary btn-sm float-left">COTIZAR ONLINE</a>
                <?php } else { ?>
                    <a href="<?php echo home_url() ?>/contacto?id=<?php echo get_the_ID(); ?>"
                       class="btn btn-primary btn-sm float-left">CONSULTA ONLINE</a>
                <?php } ?>
            <?php } ?>

            <?php  /* if (get_field('hide_landing', get_the_ID()) != 1) { ?>
                <a href="<?php the_permalink(); ?>" class="float-right anchor mt-1">Más información <i
                            class="fas fa-arrow-right"></i></a>
            <?php } */ ?>
        </div>
    </div>
</div>
