<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d9f4580b0a16',
	'title' => 'Postype : coberturas',
	'fields' => array(
		array(
			'key' => 'field_5d9f458f2b05f',
			'label' => 'Ocultar landing',
			'name' => 'hide_landing',
			'type' => 'true_false',
			'instructions' => 'Esto va a hacer que landing se muestre o no. Si lo activás solo va a aparecer en las categorías.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'acfe_permissions' => '',
			'message' => '',
			'default_value' => 0,
			'ui' => 1,
			'ui_on_text' => 'Ocultar',
			'ui_off_text' => 'Mostrar',
			'acfe_validate' => '',
			'acfe_update' => '',
		),
		array(
			'key' => 'field_5da09a976a0b7',
			'label' => 'Tipo de cobertura',
			'name' => 'insurance_type',
			'type' => 'radio',
			'instructions' => 'Esto va a modificar los datos que se pidan en los formularios de contacto, cotización o pago.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'acfe_permissions' => '',
			'choices' => array(
				'particular' => 'Particular',
				'comercial' => 'Comercial',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
			'acfe_validate' => '',
			'acfe_update' => '',
			'save_other_choice' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'cobertura',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
	'acfe_display_title' => 'Ajustes',
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_permissions' => '',
	'acfe_note' => '',
	'acfe_meta' => '',
	'modified' => 1570807111,
));

endif;