<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d95edb53aad9',
	'title' => 'Component: buttons',
	'fields' => array(
		array(
			'key' => 'field_5d95edc4351d8',
			'label' => 'Botones',
			'name' => 'cta-buttons',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'acfe_permissions' => '',
			'collapsed' => 'field_5d95ede3351d9',
			'min' => 0,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Agregar botón',
			'sub_fields' => array(
				array(
					'key' => 'field_5d95ede3351d9',
					'label' => 'Seleccionar botón',
					'name' => 'button_select',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'acfe_permissions' => '',
					'choices' => array(
						'whatsapp' => 'Whatsapp',
						'llamada' => 'Quiero que me llamen',
						'cotizar' => 'Cotizar online',
						'pedir' => 'Pedir ahora',
						'consultar' => 'Consultar online',
						'custom' => 'Custom',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'return_format' => 'value',
					'acfe_validate' => '',
					'acfe_update' => '',
					'ajax' => 0,
					'placeholder' => '',
				),
				array(
					'key' => 'field_5d9f337ab306d',
					'label' => 'Formula de cotización',
					'name' => 'cot_formula',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5d95ede3351d9',
								'operator' => '==',
								'value' => 'cotizar',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'acfe_permissions' => '',
					'choices' => array(
						1 => 'Aduana, obra o servicio',
						2 => 'Sociedades',
						3 => 'Ambiental',
						4 => 'Sin formula',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'return_format' => 'value',
					'acfe_validate' => '',
					'acfe_update' => '',
					'ajax' => 0,
					'placeholder' => '',
				),
				array(
					'key' => 'field_5d95eeafab193',
					'label' => 'Texto',
					'name' => 'button_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5d95ede3351d9',
								'operator' => '==',
								'value' => 'custom',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'acfe_permissions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'acfe_validate' => '',
					'acfe_update' => '',
				),
				array(
					'key' => 'field_5d95eec5ab194',
					'label' => 'Link',
					'name' => 'button_link',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5d95ede3351d9',
								'operator' => '==',
								'value' => 'custom',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'acfe_permissions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'acfe_validate' => '',
					'acfe_update' => '',
				),
				array(
					'key' => 'field_5d95eed9ab195',
					'label' => 'Color',
					'name' => 'button_color',
					'type' => 'select',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5d95ede3351d9',
								'operator' => '==',
								'value' => 'custom',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'acfe_permissions' => '',
					'choices' => array(
						'btn-primary' => 'Amarillo',
						'btn-info' => 'Verde',
						'btn-danger' => 'Rojo',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'return_format' => 'value',
					'acfe_validate' => '',
					'acfe_update' => '',
					'ajax' => 0,
					'placeholder' => '',
				),
			),
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/tax-heading',
			),
		),
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/help',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
	'acfe_display_title' => 'Botones',
	'acfe_autosync' => array(
		0 => 'php',
	),
	'acfe_permissions' => '',
	'acfe_note' => '',
	'acfe_meta' => '',
	'modified' => 1570823924,
));

endif;
