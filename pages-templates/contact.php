<?php
/**
 * Template Name: Formulario de contacto
 *
 */
?>
<?php
// Tomamos el id
$post_id = $_GET['id'];
$action = $_GET['action'];

if (!empty($post_id)) {
// Detectamos a que categoría pertenece
// Si es una página, entonces viene de una categoria... hay que hacerle algunas preguntas.
    $insurance_type = "particular";

    if (get_post_type($post_id) == 'page') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = get_terms('cobertura_tax', array(
            'hide_empty' => false,
        ));

        //$insurance = $post_id;
        $insurance_name = get_the_title($post_id);
        $is_page = true;

        foreach ($terms as $term) {
            $tax_page = get_field('tax_pagina', 'cobertura_tax_' . $term->term_id);
            if ($tax_page == $post_id) {
                $insurance = $term->term_id;
                $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            }else{
                $is_not_term = true;
            }
        }
    }


    if (get_post_type($post_id) == 'cobertura') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = wp_get_post_terms($post_id, 'cobertura_tax');
        foreach ($terms as $term) {
            $insurance = $term->term_id;
            $insurance_tax = $term->name;
            $insurance_selection = $post_id;
            $insurance_name = get_the_title($post_id);
            $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            $opt = $_GET['opt'];
        }
        if (get_field('insurance_type', $post_id)) {
            $insurance_type = get_field('insurance_type');
        }
    }
if ($insurance) {
    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura',
        'tax_query' => array(
            array(
                'taxonomy' => 'cobertura_tax',
                'field' => 'term_id',
                'terms' => $insurance,
            )
        )
    );
}else{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura'
    );
}

    $coverages = get_posts($args);

    // Traemos los datos del vendedor
    if (empty($sellers)) {
        $args = array(
            'numberposts' => 1,
            'post_type' => 'vendedor'
        );
        $sellers = get_posts($args);
    }
    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
} else {
    //traemos el primer vendedor
    $args = array(
        'numberposts' => 1,
        'post_type' => 'vendedor'
    );
    $sellers = get_posts($args);

    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura'
    );
    $coverages = get_posts($args);

}
?>

<?php get_header(); ?>

<div class="wrapper" id="single-wrapper">
    <div id="content" tabindex="-1">
        <main class="site-main form-sell" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="navbar-nav text-dark nav-navigation">
                            <a href="#" @click="goBack()" class="text-dark">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-8 mt-4 text-center">
                        <div class="seller">
                            <div class="seller-image"
                                 style="background-image: url(<?php echo $seller_image_url; ?>)"></div>
                        </div>
                        <form @submit.prevent="submit">
                            <!-- STEP 1 -->
                            <input type="hidden" name="insurance_selected" value="<?php echo $insurance_name; ?>">
                            <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
                            <input type="hidden" name="get_action" value="<?php echo $action; ?>">
                            <input type="hidden" name="insurance_id" v-model="form.insurance_id">

                            <input type="hidden" name="seller" value="<?php echo $seller_name; ?>">
                            <input type="hidden" name="seller_email" value="<?php echo $seller_email; ?>">

                            <transition name="fade">
                                <div class="form-block" v-if="step == 1">
                                    <div class="seller-text">
                                        <h2>¡Hola! soy <?php echo $seller_name; ?></h2>
                                        <p class="lead">
                                            Estoy para responder todas tus dudas
                                        </p>
                                    </div>
                                    <div class="row mt-4 justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="email">Decime tu nombre</label>
                                                <input type="text"
                                                       v-model="form.name"
                                                       v-validate="'required'"
                                                       id="name"
                                                       name="name"
                                                       class="form-control"
                                                       data-vv-name="nombre"
                                                       :class="{ 'is-invalid': errors.has('nombre') }"/>
                                                <div v-if="errors.has('nombre')" class="invalid-feedback">
                                                    {{ errors.first('nombre') }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Y tu apellido</label>
                                                <input type="text"
                                                       v-model="form.surname"
                                                       v-validate="'required'"
                                                       id="surname"
                                                       name="surname"
                                                       class="form-control"
                                                       data-vv-name="apellido"
                                                       :class="{ 'is-invalid': errors.has('apellido') }"/>
                                                <div v-if="errors.has('apellido')" class="invalid-feedback">
                                                    {{ errors.first('apellido') }}
                                                </div>
                                            </div>

                                            <button class="btn btn-primary btn-block btn-lg px-4"
                                                    @click.stop.prevent="goToStep('2')">
                                                SIGUIENTE
                                            </button>

                                        </div>
                                    </div>

                                </div>
                            </transition>


                            <transition name="fade">
                                <!-- COTIZACIÓN -->
                                <div class="form-block" v-if="step == 2">
                                    <div class="seller-text">
                                        <h2><?php if ($seller_gender == 'fem') {
                                                echo 'Encantada';
                                            } else {
                                                echo 'Encantado';
                                            } ?> {{form.name}},</h2>
                                        <p class="lead">
                                            <?php

                                            if ($insurance!="" && $is_not_term!=true) { ?>
                                                ¿Qué tipo de <?php echo $insurance_name ?>
                                                necesitás?
                                            <?php } elseif ($post_id != "" && $is_not_term!=true) { ?>
                                                ¿Qué tipo de <?php echo $insurance_tax ?>
                                                necesitás?
                                            <?php } else { ?>
                                                ¿Qué tipo de cobertura
                                                necesitás?
                                            <?php } ?>
                                        </p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="insurance">Elegí una opción</label>
                                                <select
                                                        class="form-control"
                                                        id="insurance"
                                                        v-validate="'required'"
                                                        v-model="form.insurance_id"
                                                        data-vv-name="cobertura"
                                                        :class="{ 'is-invalid': errors.has('cobertura') }">

                                                    <option value="">Seleccionar</option>
                                                    <?php
                                                    foreach ($coverages as $coverage) { ?>
                                                        <option value="<?php echo $coverage->ID ?>">
                                                            <?php echo $coverage->post_title; ?>
                                                        </option>
                                                    <?php }

                                                    ?>
                                                </select>
                                                <div v-if="errors.has('cobertura')" class="invalid-feedback">
                                                    {{ errors.first('cobertura') }}
                                                </div>
                                            </div>
                                            <div class="text-center form-block-actions">
                                                <button class="btn btn-primary btn-block btn-lg px-4"
                                                        @click.stop.prevent="goToStep('3')">
                                                    SIGUIENTE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>


                            <!-- CONTACTO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 3">
                                    <div class="seller-text">
                                        <?php if (!$action){ ?>
                                        <h2>¿Cómo preferís que te contactemos para avanzar?</h2>
                                        <?php }else{ ?>
                                            <h2>Ahora si, dejame tu número de teléfono</h2>
                                        <?php } ?>
                                    </div>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="customradio">
                                                <?php if (!$action){ ?>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_1"
                                                           value="whatsapp"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_1">Whatsapp</label>
                                                    <input type="text"
                                                           name="contact-whatsapp"
                                                           class="form-control"
                                                           v-if="form.contactMode=='whatsapp'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de Whatsapp"
                                                           :class="{ 'is-invalid': errors.has('número de Whatsapp') }"
                                                    >
                                                    <div v-if="errors.has('número de Whatsapp')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de Whatsapp') }}
                                                    </div>

                                                </div>
                                                <?php } ?>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <?php if (!$action){ ?>
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_2"
                                                           value="phone"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_2">Teléfono</label>
                                                    <?php } ?>
                                                    <input type="text"
                                                           name="contact-phone"
                                                           class="form-control"
                                                           v-if="form.contactMode=='phone'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de teléfono"
                                                           :class="{ 'is-invalid': errors.has('número de teléfono') }"
                                                    >
                                                    <div v-if="errors.has('número de teléfono')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de teléfono') }}
                                                    </div>
                                                </div>
                                                <?php if (!$action){ ?>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           id="radio_3"
                                                           name="radio_group"
                                                           value="email"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_3">Email</label>

                                                    <input type="email"
                                                           name="contact-email"
                                                           class="form-control"
                                                           v-if="form.contactMode=='email'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá tu email"
                                                           v-validate="'required|email'"
                                                           data-vv-name="email"
                                                           :class="{ 'is-invalid': errors.has('email') }"
                                                    >
                                                    <div v-if="errors.has('email')" class="invalid-feedback">
                                                        {{ errors.first('email') }}
                                                    </div>
                                                </div>
                                                <div v-if="errors.has('modo de contacto')"
                                                     class="d-block invalid-feedback">
                                                    {{ errors.first('modo de contacto') }}
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <p class="lead mt-4 mb-3">
                                                Algunos datos más
                                            </p>
                                            <?php if ($insurance_type != 'comercial') { ?>
                                                <div class="form-group">
                                                    <label for="dni">DNI</label>
                                                    <input type="tel"
                                                           v-model="form.dni"
                                                           v-validate="'required|numeric|max:8|min:7'"
                                                           id="dni"
                                                           name="dni"
                                                           class="form-control"
                                                           data-vv-name="DNI"
                                                           :class="{ 'is-invalid': errors.has('DNI') }"/>
                                                    <div v-if="errors.has('DNI')" class="invalid-feedback">
                                                        {{ errors.first('DNI') }}
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="form-group">
                                                    <label for="dni">CUIT</label>
                                                    <input type="text"
                                                           v-model="form.cuit"
                                                           v-validate="'required|numeric'"
                                                           id="cuit"
                                                           name="cuit"
                                                           v-mask="'##-########-#'"
                                                           class="form-control"
                                                           data-vv-name="CUIT"
                                                           :class="{ 'is-invalid': errors.has('CUIT') }"/>
                                                    <div v-if="errors.has('CUIT')" class="invalid-feedback">
                                                        {{ errors.first('CUIT') }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bussines_name">Razón social</label>
                                                    <input type="text"
                                                           v-model="form.bussines_name"
                                                           v-validate="'required'"
                                                           id="bussines_name"
                                                           name="bussines_name"
                                                           class="form-control"
                                                           data-vv-name="razón social"
                                                           :class="{ 'is-invalid': errors.has('razón social') }"/>
                                                    <div v-if="errors.has('razón social')" class="invalid-feedback">
                                                        {{ errors.first('razón social') }}
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label for="comments">Contános un poco más</label>
                                                <textarea
                                                        v-model="form.comments"
                                                        v-validate=""
                                                        id="comments"
                                                        name="comments"
                                                        class="form-control"
                                                        data-vv-name="comentarios"
                                                        :class="{ 'is-invalid': errors.has('comentarios') }"
                                                >
                                                </textarea>
                                                <div v-if="errors.has('comentarios')" class="invalid-feedback">
                                                    {{ errors.first('comentarios') }}
                                                </div>

                                            </div>
                                            <button class="btn btn-primary btn-block btn-lg px-4"
                                                    @click.stop.prevent="goToStep('submit')">
                                                <i class="fas fa-circle-notch fa-spin" v-if="loading"></i> ENVIAR
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </transition>

                            <!-- OFICINAS -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'success'">
                                    <div class="seller-text">
                                        <h2>Gracias {{form.name}}</h2>
                                        <p class="lead">Recibimos tu pedido. Nos comunicaremos con vos para darle
                                            seguimiento.</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'error'">
                                    <div class="seller-text">
                                        <h2>Lo siento {{form.name}}, hubo un error en el envio</h2>
                                        <p class="lead">Por favor intenta nuevamente más tarde</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <input type="hidden" name="error" v-if="form.price == '' || form.insurance ==''"
                                   v-model="form.error" value="No se pudo realizar el pago.">

                            <button class="btn btn-link" @click="prev()" v-if="step > 1 && step < 4"><i
                                        class="fas fa-arrow-left"></i> Anterior
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- .row -->
</div><!-- #content -->
<?php get_footer(); ?>
<script>
    Vue.use(VeeValidate);
    VeeValidate.Validator.localize("es");
    Vue.use(VueMask.VueMaskPlugin);

    const app = new Vue({
        el: 'main',
        data: {
            step: '1',
            success: false,
            loading: false,
            cotization: '',
            form: {
                name: '',
                surname: '',
                dni: '',
                cuit: '',
                insurance: '',
                insurance_id: 0,
                post_id: '',
                bussines_name: '',
                contactMode: '',
                contactData: '',
                action: '',
                error: '',
                seller: '',
                seller_email: ''
            },
        },
        methods: {
            goBack() {
                window.history.back();
            },
            goToStep(step) {
                this.$validator.validate().then(valid => {
                    if (valid) {
                        window.scrollTo({top: 0, behavior: 'smooth'});
                        if (step == 'submit') {
                            this.submit()
                        } else {
                            this.step = step;
                        }
                    }
                })
            },
            prev() {
                this.step--;
                window.scrollTo({top: 0, behavior: 'smooth'});
            },
            next() {
                this.step++;
                window.scrollTo({top: 0, behavior: 'smooth'});
            },
            submit: async function () {
                this.loading = true;
                try {
                    const {data} = await axios.post('http://woranz.com/bin/contact-send.php', this.form);
                    this.goToStep('success');
                    console.log(data);
                    this.loading = false;
                } catch (e) {
                    console.log(e);
                    this.loading = false;
                    this.goToStep('error');
                }
            },
            toFormData: function (obj) {
                let formData = new FormData();
                for (let key in obj) {
                    formData.append(key, obj[key]);
                }
                return formData;
            }
        },
        beforeMount: function () {
            this.form.insurance = this.$el.querySelector('input[name=insurance_selected]').value;
            this.form.seller = this.$el.querySelector('input[name=seller]').value;
            this.form.seller_email = this.$el.querySelector('input[name=seller_email]').value;
            this.form.insurance_id = this.$el.querySelector('input[name=post_id]').value;
            let check_id = false;
            for (i = 0; i < document.getElementById("insurance").length; ++i){
                if (document.getElementById("insurance").options[i].value ==  this.form.insurance_id){
                    check_id =true;
                }
            }
            if (check_id==false){
                this.form.insurance_id = '';
            }
            this.form.action = this.$el.querySelector('input[name=get_action]').value;
            if (this.form.action!=''){
                this.form.contactMode='phone';
            }
            console.log(this.form);
        }
    });
</script>
