<?php
/**
 * Template Name: Formulario de cotizacion
 *
 */
?>
<?php
// Tomamos el id
$post_id = $_GET['id'];

if (!empty($post_id)) {
// Detectamos a que categoría pertenece
// Si es una página, entonces viene de una categoria... hay que hacerle algunas preguntas.
    if (get_post_type($post_id) == 'page') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = get_terms('cobertura_tax', array(
            'hide_empty' => false,
        ));

        $insurance = $post_id;
        $insurance_name = get_the_title($post_id);
        $is_page = true;

        foreach ($terms as $term) {
            $tax_page = get_field('tax_pagina', 'cobertura_tax_' . $term->term_id);
            if ($tax_page == $post_id) {
                $insurance = $term->term_id;
                $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            }
        }
    }


    if (get_post_type($post_id) == 'cobertura') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = wp_get_post_terms($post_id, 'cobertura_tax');
        foreach ($terms as $term) {
            $insurance = $term->term_id;
            $insurance_tax = $term->name;
            $insurance_selection = $post_id;
            $insurance_name = get_the_title($post_id);
            $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            $opt = $_GET['opt'];
        }
    }
    $cotization_formula = get_cotization($post_id);
    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura',
        'tax_query' => array(
            array(
                'taxonomy' => 'cobertura_tax',
                'field' => 'term_id',
                'terms' => $insurance,
            )
        )
    );
    $coverages = get_posts($args);

    // Traemos los datos del vendedor
    if (empty($sellers)) {
        $args = array(
            'numberposts' => 1,
            'post_type' => 'vendedor'
        );
        $sellers = get_posts($args);
    }
    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
} else {
    //traemos el primer vendedor
    $args = array(
        'numberposts' => 1,
        'post_type' => 'vendedor'
    );
    $sellers = get_posts($args);
    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
}
?>

<?php get_header(); ?>

<div class="wrapper" id="single-wrapper">
    <div id="content" tabindex="-1">
        <main class="site-main form-sell" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="navbar-nav text-dark nav-navigation">
                            <a href="#" @click="goBack()" class="text-dark">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-8 mt-4 text-center">
                        <div class="seller">
                            <div class="seller-image"
                                 style="background-image: url(<?php echo $seller_image_url; ?>)"></div>
                        </div>
                        <form @submit.prevent="submit">
                            <!-- STEP 1 -->

                                <input type="hidden" name="insurance_selected" value="<?php echo $insurance_name; ?>">

                            <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
                            <input type="hidden" name="insurance_id" v-model="form.insurance_id">

                            <input type="hidden" name="seller" value="<?php echo $seller_name; ?>">
                            <input type="hidden" name="seller_email" value="<?php echo $seller_email; ?>">

                            <transition name="fade">
                                <div class="form-block" v-if="step == 1">
                                    <div class="seller-text">
                                        <h2>¡Hola! soy <?php echo $seller_name; ?></h2>
                                        <p class="lead">
                                            Voy a ayudarte a cotizar tu póliza
                                        </p>
                                    </div>
                                    <div class="row mt-4 justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="email">Decime tu nombre</label>
                                                <input type="text"
                                                       v-model="form.name"
                                                       v-validate="'required'"
                                                       id="name"
                                                       name="name"
                                                       class="form-control"
                                                       data-vv-name="nombre"
                                                       :class="{ 'is-invalid': errors.has('nombre') }"/>
                                                <div v-if="errors.has('nombre')" class="invalid-feedback">
                                                    {{ errors.first('nombre') }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Y tu apellido</label>
                                                <input type="text"
                                                       v-model="form.surname"
                                                       v-validate="'required'"
                                                       id="surname"
                                                       name="surname"
                                                       class="form-control"
                                                       data-vv-name="apellido"
                                                       :class="{ 'is-invalid': errors.has('apellido') }"/>
                                                <div v-if="errors.has('apellido')" class="invalid-feedback">
                                                    {{ errors.first('apellido') }}
                                                </div>
                                            </div>
                                            <?php if (count($coverages) > 0) { ?>
                                                <button class="btn btn-primary btn-block btn-lg px-4"
                                                        @click.stop.prevent="goToStep('2')">
                                                    SIGUIENTE
                                                </button>
                                            <?php } else { ?>
                                                <button class="btn btn-primary btn-block btn-lg px-4"
                                                        @click.stop.prevent="goToStep('3')">
                                                    SIGUIENTE
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                            </transition>

                            <?php if (count($coverages) > 0) { ?>
                                <transition name="fade">
                                    <!-- COTIZACIÓN -->
                                    <div class="form-block" v-if="step == 2">
                                        <div class="seller-text">
                                            <h2><?php if ($seller_gender == 'fem') {
                                                    echo 'Encantada';
                                                } else {
                                                    echo 'Encantado';
                                                } ?> {{form.name}},</h2>
                                            <p class="lead">
                                                <?php

                                                if ($is_page) { ?>
                                                    ¿Qué tipo de <?php echo $insurance_name ?>
                                                    necesitás?
                                                <?php } else { ?>
                                                    ¿Qué tipo de <?php echo $insurance_tax ?>
                                                    necesitás?
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="insurance">Elegí una opción</label>
                                                    <select
                                                            class="form-control"
                                                            id="insurance"
                                                            v-validate="'required'"
                                                            v-model="form.insurance_id"
                                                            data-vv-name="cobertura"
                                                            :class="{ 'is-invalid': errors.has('cobertura') }">

                                                        <option value="">Seleccionar</option>
                                                        <?php
                                                        foreach ($coverages as $coverage) { ?>
                                                            <option value="<?php echo $coverage->ID ?>">
                                                                <?php echo $coverage->post_title; ?>
                                                            </option>
                                                        <?php }

                                                        ?>
                                                    </select>
                                                    <div v-if="errors.has('cobertura')" class="invalid-feedback">
                                                        {{ errors.first('cobertura') }}
                                                    </div>
                                                </div>
                                                <div class="text-center form-block-actions">
                                                    <button class="btn btn-primary btn-block btn-lg px-4"
                                                            @click.stop.prevent="goToStep('3')">
                                                        SIGUIENTE
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </transition>
                            <?php } ?>

                            <transition name="fade">
                                <div class="form-block" v-if="step == 3">
                                    <div class="seller-text">
                                        <h2>
                                            Último paso
                                        </h2>
                                        <p class="lead">
                                            Te pido algunos datos para poder cotizar tu póliza
                                        </p>
                                    </div>
                                    <div class="row mt-4 justify-content-center">
                                        <div class="col-lg-6">
                                            <?php if ($cotization_formula == '1') { ?>
                                                <div class="form-group">
                                                    <label for="contract_amount">¿Cuál es el monto del contrato?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">$</div>
                                                        </div>
                                                        <input type="text"
                                                               v-model="form.contract_amount"
                                                               v-validate="'required|numeric'"
                                                               id="contract_amount"
                                                               name="contract_amount"
                                                               class="form-control"
                                                               data-vv-name="monto del contrato"
                                                               :class="{ 'is-invalid': errors.has('monto del contrato') }"/>
                                                    </div>
                                                    <div v-if="errors.has('monto del contrato')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('monto del contrato') }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sum_assured">¿Qué suma necesitás asegurar?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">$</div>
                                                        </div>
                                                        <input type="text"
                                                               v-model="form.sum_assured"
                                                               v-validate="'required|numeric'"
                                                               id="sum_assured"
                                                               name="sum_assured"
                                                               class="form-control"
                                                               data-vv-name="suma asegurada"
                                                               :class="{ 'is-invalid': errors.has('suma asegurada') }"/>
                                                    </div>
                                                    <div v-if="errors.has('suma asegurada')" class="invalid-feedback">
                                                        {{ errors.first('suma asegurada') }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sum_assured">¿Cuál es el patrimonio neto de tu
                                                        empresa?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">$</div>
                                                        </div>
                                                        <input type="number"
                                                               v-model="form.equity"
                                                               v-validate="'required|numeric'"
                                                               id="equity"
                                                               name="equity"
                                                               class="form-control money"
                                                               data-vv-name="patrimonio"
                                                               :class="{ 'is-invalid': errors.has('patrimonio') }"/>
                                                    </div>
                                                    <div v-if="errors.has('patrimonio')" class="invalid-feedback">
                                                        {{ errors.first('patrimonio') }}
                                                    </div>
                                                </div>
                                            <?php } elseif ($cotization_formula == 2) { ?>
                                                <div class="form-group">
                                                    <label for="capital">¿Cuanto es capital que necesitás
                                                        asegurar?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">$</div>
                                                        </div>
                                                        <input type="text"
                                                               v-model="form.capital"
                                                               v-validate="'required'"
                                                               id="capital"
                                                               name="capital"
                                                               class="form-control money"
                                                               data-vv-name="capital"
                                                               :class="{ 'is-invalid': errors.has('capital') }"/>
                                                    </div>
                                                    <div v-if="errors.has('capital')" class="invalid-feedback">
                                                        {{ errors.first('capital') }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="partners">Cantidad de socios</label>
                                                    <input type="number"
                                                           v-model="form.partners"
                                                           v-validate="'required|numeric'"
                                                           id="partners"
                                                           name="partners"
                                                           class="form-control"
                                                           data-vv-name="socios"
                                                           :class="{ 'is-invalid': errors.has('socios') }"/>
                                                    <div v-if="errors.has('socios')" class="invalid-feedback">
                                                        {{ errors.first('socios') }}
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="form-group">
                                                    <label for="cuit">CUIT de tu empresa</label>
                                                    <input type="text"
                                                           v-model="form.cuit"
                                                           v-validate="'required'"
                                                           id="cuit"
                                                           name="cuit"
                                                           v-mask="'##-########-#'"
                                                           class="form-control"
                                                           data-vv-name="CUIT"
                                                           :class="{ 'is-invalid': errors.has('CUIT') }"/>
                                                    <div v-if="errors.has('CUIT')" class="invalid-feedback">
                                                        {{ errors.first('CUIT') }}
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="bussines_name">Razón social</label>
                                                    <input type="text"
                                                           v-model="form.bussines_name"
                                                           v-validate="'required'"
                                                           id="bussines_name"
                                                           name="bussines_name"
                                                           class="form-control"
                                                           data-vv-name="razón social"
                                                           :class="{ 'is-invalid': errors.has('razón social') }"/>
                                                    <div v-if="errors.has('razón social')" class="invalid-feedback">
                                                        {{ errors.first('razón social') }}
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="text-center form-block-actions">
                                                <button class="btn btn-primary btn-block btn-lg px-4"
                                                        @click.stop.prevent="cotizar()">
                                                    <i class="fas fa-circle-notch fa-spin" v-if="loading"></i> COTIZAR
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </transition>

                            <!-- SI LA COBERTURA TIENE MÁS DE UNA OPCIÓN -->

                            <?php /*if (is_array(get_price($post_id))){ ?>
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'price_select'">
                                    <div class="seller-text">
                                        <h2>¿Qué tipo de cobertura necesitás?</h2>
                                    </div>

                                    <?php
                                    $coverages_prices= get_price($post_id);
                                    ?>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="insurance">Elegí una opción</label>
                                                <div class="customradio">
                                                <?php $i=0; ?>
                                               <?php  foreach ($coverages_prices as $key => $value) { ?>

                                                    <?php

                                                        if ($key=='list-prices-'.$i.'_list_price_name'){
                                                            $name = 'list-prices-'.$i.'_list_price_name';
                                                        }

                                                   ?>

                                                    <div class="customradio-primary"
                                                         :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                        <input type="radio"
                                                               name="radio_group"
                                                               id="radio_1"
                                                               value="whatsapp"
                                                               v-model="form.contactMode"
                                                               v-validate="'required'"
                                                               data-vv-name="modo de contacto"
                                                        />
                                                        <label for="radio_1"></label>

                                                        <div v-if="errors.has('número de Whatsapp')"
                                                             class="invalid-feedback">
                                                            {{ errors.first('número de Whatsapp') }}
                                                        </div>
                                                    </div>
                                                <?php echo $key.': '.$value; ?><br>
                                                <?php } ?>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="text-center form-block-actions">
                                        <button class="btn btn-primary btn-lg px-4"
                                                @click.stop.prevent="goToStep('dni')">
                                            SIGUIENTE
                                        </button>
                                    </div>
                                </div>
                            </transition>
                            <?php } */ ?>

                            <!-- CONTACTO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 4">
                                    <div class="seller-text" v-if="cotization != 'error'">
                                        <h2>
                                            El precio de tu póliza es
                                            <span class="text-info">
                                                {{cotization}}
                                             </span>
                                            <?php if ($cotization_formula == 2) { ?>
                                                <br> por socio asegurado
                                            <?php } ?>
                                        </h2>

                                        <p class="lead">
                                            ¿Te gustaría que te contactemos para hacerla efectiva?
                                        </p>
                                    </div>
                                    <div class="seller-text" v-else>
                                        <h2>
                                            Voy a necesitar algunos datos más para cotizar tu póliza
                                        </h2>
                                        <p class="lead mb-0">
                                            ¿Por qué medio preferís que hablemos?
                                        </p>
                                    </div>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="customradio">
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_1"
                                                           value="whatsapp"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_1">Whatsapp</label>
                                                    <input type="text"
                                                           name="contact-whatsapp"
                                                           class="form-control"
                                                           v-if="form.contactMode=='whatsapp'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de Whatsapp"
                                                           :class="{ 'is-invalid': errors.has('número de Whatsapp') }"
                                                    >
                                                    <div v-if="errors.has('número de Whatsapp')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de Whatsapp') }}
                                                    </div>
                                                </div>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_2"
                                                           value="phone"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_2">Teléfono</label>
                                                    <input type="text"
                                                           name="contact-phone"
                                                           class="form-control"
                                                           v-if="form.contactMode=='phone'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de teléfono"
                                                           :class="{ 'is-invalid': errors.has('número de teléfono') }"
                                                    >
                                                    <div v-if="errors.has('número de teléfono')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de teléfono') }}
                                                    </div>
                                                </div>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           id="radio_3"
                                                           name="radio_group"
                                                           value="email"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_3">Email</label>

                                                    <input type="email"
                                                           name="contact-email"
                                                           class="form-control"
                                                           v-if="form.contactMode=='email'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá tu email"
                                                           v-validate="'required|email'"
                                                           data-vv-name="email"
                                                           :class="{ 'is-invalid': errors.has('email') }"
                                                    >
                                                    <div v-if="errors.has('email')" class="invalid-feedback">
                                                        {{ errors.first('email') }}
                                                    </div>
                                                </div>
                                                <div v-if="errors.has('modo de contacto')"
                                                     class="d-block invalid-feedback">
                                                    {{ errors.first('modo de contacto') }}
                                                </div>
                                            </div>
                                            <?php if ($cotization_formula != 1 and $cotization_formula != 2 and $cotization_formula != '') { ?>

                                                <p class="lead mt-3 mb-3">
                                                    Algunos datos más:
                                                </p>

                                                <div class="form-group">
                                                    <label for="cuit">CUIT de tu empresa</label>
                                                    <input type="text"
                                                           v-model="form.cuit"
                                                           v-validate="'required'"
                                                           id="cuit"
                                                           name="cuit"
                                                           v-mask="'##-########-#'"
                                                           class="form-control"
                                                           data-vv-name="CUIT"
                                                           :class="{ 'is-invalid': errors.has('CUIT') }"/>
                                                    <div v-if="errors.has('CUIT')" class="invalid-feedback">
                                                        {{ errors.first('CUIT') }}
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="bussines_name">Razón social</label>
                                                    <input type="text"
                                                           v-model="form.bussines_name"
                                                           v-validate="'required'"
                                                           id="bussines_name"
                                                           name="bussines_name"
                                                           class="form-control"
                                                           data-vv-name="razón social"
                                                           :class="{ 'is-invalid': errors.has('razón social') }"/>
                                                    <div v-if="errors.has('razón social')" class="invalid-feedback">
                                                        {{ errors.first('razón social') }}
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <button class="btn btn-primary btn-block btn-lg px-4"
                                                    @click.stop.prevent="goToStep('submit')">
                                                <i class="fas fa-circle-notch fa-spin" v-if="loading"></i> ENVIAR
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- OFICINAS -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'success'">
                                    <div class="seller-text">
                                        <h2>Gracias {{form.name}}</h2>
                                        <p class="lead">Recibimos tu pedido. Nos comunicaremos con vos para darle
                                            seguimiento.</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'error'">
                                    <div class="seller-text">
                                        <h2>Lo siento {{form.name}}, hubo un error con la cotizacion</h2>
                                        <p class="lead">Nos comunicaremos con vos para darle seguimiento</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <input type="hidden" name="error" v-if="form.price == '' || form.insurance ==''"
                                   v-model="form.error" value="No se pudo realizar el pago.">

                            <button class="btn btn-link" @click="prev()" v-if="step > 1 && step < 4"><i
                                        class="fas fa-arrow-left"></i> Anterior
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- .row -->
</div><!-- #content -->
<?php get_footer(); ?>
<script>
    Vue.use(VeeValidate);
    VeeValidate.Validator.localize("es");
    Vue.use(VueMask.VueMaskPlugin);

    const app = new Vue({
        el: 'main',
        data: {
            step: '1',
            success: false,
            loading: false,
            cotization: '',
            form: {
                name: '',
                surname: '',
                cuit: '',
                insurance: '',
                insurance_id: '',
                post_id: '',
                bussines_name: '',
                contract_amount: '',
                sum_assured: '',
                price: '',
                contactMode: '',
                contactData: '',
                error: '',
                seller: '',
                seller_email: ''
            },
        },
        methods: {
            goBack() {
                window.history.back();
            },
            goToStep(step) {
                this.$validator.validate().then(valid => {
                    if (valid) {
                        window.scrollTo({top: 0, behavior: 'smooth'});
                        if (step == 'submit') {
                            this.submit()
                        } else {
                            this.step = step;
                        }
                    }
                })
            },


            prev() {
                this.step--;
                window.scrollTo({top: 0, behavior: 'smooth'});
            },
            next() {
                this.step++;
                window.scrollTo({top: 0, behavior: 'smooth'});
            },
            cotizar: async function () {
                this.loading = true;
                try {
                    const {data} = await axios.post('http://c1440028.ferozo.com/bin/cotization.php', this.form);
                    this.goToStep(4);
                    this.cotization = "$ " + data.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\,\d+)?$)/g, "$1.");
                    this.form.price= data;
                    console.log(data);
                    this.loading = false;
                } catch (e) {
                    this.goToStep(4);
                    this.cotization = 'error';
                    this.loading = false;
                }
            },
            submit: async function () {
                this.loading = true;
                try {
                    const {data} = await axios.post('http://woranz.com/bin/cotization-send.php', this.form);
                    this.goToStep('success');
                    console.log(data);
                    this.loading = false;
                } catch (e) {
                    this.loading = false;
                    this.goToStep('error');
                }
            },
            toFormData: function (obj) {
                let formData = new FormData();
                for (let key in obj) {
                    formData.append(key, obj[key]);
                }
                return formData;
            }
        },
        beforeMount: function () {
            this.form.insurance = this.$el.querySelector('input[name=insurance_selected]').value;
            this.form.seller = this.$el.querySelector('input[name=seller]').value;
            this.form.seller_email = this.$el.querySelector('input[name=seller_email]').value;
            this.form.insurance_id = this.$el.querySelector('input[name=post_id]').value;
            let check_id = false;
            for (i = 0; i < document.getElementById("insurance").length; ++i){
                if (document.getElementById("insurance").options[i].value ==  this.form.insurance_id){
                    check_id =true;
                }
            }
            if (check_id==false){
                this.form.insurance_id = '';
            }
            console.log(this.form);
        }
    });
</script>
