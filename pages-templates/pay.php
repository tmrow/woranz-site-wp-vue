<?php
/**
 * Template Name: Formulario de pago
 *
 */
?>
<?php


// Tomamos el id
$post_id = $_GET['id'];
$action = $_GET['action'];
$message = $_GET['message'];

if (!empty($post_id)) {
// Detectamos a que categoría pertenece
// Si es una página, entonces viene de una categoria... hay que hacerle algunas preguntas.

    if (get_post_type($post_id) == 'page') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = get_terms('cobertura_tax', array(
            'hide_empty' => false,
        ));

        $insurance = $post_id;
        $insurance_name = get_the_title($post_id);

        foreach ($terms as $term) {
            $tax_page = get_field('tax_pagina', 'cobertura_tax_' . $term->term_id);
            if ($tax_page == $post_id) {
                //esta es la categoria
                $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            }
        }
    }

    if (get_post_type($post_id) == 'cobertura') {
        // Recorremos las categorias y detectamos cual está vinculada con esta página
        $terms = wp_get_post_terms($post_id, 'cobertura_tax');
        foreach ($terms as $term) {
            $insurance = $term->term_id;
            $insurance_selection = $post_id;
            $insurance_name = get_the_title($post_id);
            $sellers = get_field('tax_seller', 'cobertura_tax_' . $term->term_id);
            $opt = $_GET['opt'];
            //$insurance_price= get_price($post_id);
        }
    }

    if (get_field('insurance_type', $post_id)) {
        $insurance_type = get_field('insurance_type');
    }else{
        $insurance_type = 'particular';
    }

    if ($opt != "") {
        $price = get_price($post_id, $opt);
    } else {
        $price = get_price($post_id, '');
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura',
        'tax_query' => array(
            array(
                'taxonomy' => 'cobertura_tax',
                'field' => 'term_id',
                'terms' => $insurance,
            )
        )
    );

    $coverages = get_posts($args);

    // Traemos los datos del vendedor
    if (empty($sellers)) {
        $args = array(
            'numberposts' => 1,
            'post_type' => 'vendedor'
        );
        $sellers = get_posts($args);
    }
    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
} else {
    //traemos el primer vendedor
    $args = array(
        'numberposts' => 1,
        'post_type' => 'vendedor'
    );
    $insurance_type = "particular";
    $sellers = get_posts($args);
    foreach ($sellers as $seller) {
        $seller_name = $seller->post_title;
        $seller_image = get_the_post_thumbnail($seller->ID);
        $seller_image_url = get_the_post_thumbnail_url($seller->ID);
        $seller_gender = get_field('seller_gender', $seller->ID);
        $seller_email = get_field('seller_email', $seller->ID);
    }
    $args = array(
        'numberposts' => -1,
        'post_type' => 'cobertura'
    );

    $coverages = get_posts($args);
}
?>

<?php get_header(); ?>

<div class="wrapper" id="single-wrapper">
    <div id="content" tabindex="-1">
        <main class="site-main form-sell" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="navbar-nav text-dark">
                            <a href="#" @click="goBack()" class="text-dark">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-8 mt-4 text-center">
                        <div class="seller">
                            <div class="seller-image"
                                 style="background-image: url(<?php echo $seller_image_url; ?>)"></div>
                        </div>
                        <form @submit.prevent="submit">
                            <!-- STEP 1 -->
                            <input type="hidden" name="insurance_selected" value="<?php echo $insurance_name; ?>">
                            <input type="hidden" name="price" value="<?php echo $price[0]; ?>">
                            <input type="hidden" name="seller" value="<?php echo $seller_name; ?>">
                            <input type="hidden" name="seller_email" value="<?php echo $seller_email; ?>">
                            <input type="hidden" name="get_error" value="<?php echo $_GET['message']; ?>">

                            <transition name="fade">
                                <div class="form-block" v-if="step == 1">
                                    <div class="seller-text">
                                        <h2>¡Hola! soy <?php echo $seller_name; ?></h2>
                                        <p class="lead">
                                                Voy a ayudarte para que tengas tu seguro lo más rápido posible
                                        </p>
                                    </div>
                                    <div class="row justify-content-center mt-4">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="email">Decime tu nombre</label>
                                                <input type="text"
                                                       v-model="form.name"
                                                       v-validate="'required'"
                                                       id="name"
                                                       name="name"
                                                       class="form-control"
                                                       data-vv-name="nombre"
                                                       :class="{ 'is-invalid': errors.has('nombre') }"/>
                                                <div v-if="errors.has('nombre')" class="invalid-feedback">
                                                    {{ errors.first('nombre') }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Y tu apellido</label>
                                                <input type="text"
                                                       v-model="form.surname"
                                                       v-validate="'required'"
                                                       id="surname"
                                                       name="surname"
                                                       class="form-control"
                                                       data-vv-name="apellido"
                                                       :class="{ 'is-invalid': errors.has('apellido') }"/>
                                                <div v-if="errors.has('apellido')" class="invalid-feedback">
                                                    {{ errors.first('apellido') }}
                                                </div>
                                            </div>
                                            <button class="btn btn-primary btn-lg px-4"
                                                    @click.stop.prevent="goToStep('2')">
                                                SIGUIENTE
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </transition>


                            <!-- STEP 2 -->
                            <!-- SI YA SELECCIONÓ LA OPCIÓN -->
                            <transition name="fade">
                                <!-- CAUCIONES DE ALQUILER -->
                                <div class="form-block" v-if="step == 2">
                                    <div class="seller-text">
                                        <h2>
                                            <?php if ($seller_gender == 'fem') {
                                                echo 'Encantada';
                                            } else {
                                                echo 'Encantado';
                                            } ?> {{form.name}}
                                        </h2>
                                        <p class="lead">
                                            <big>
                                                Te pido algunos datos para poder avanzar con tu póliza
                                            </big>
                                        </p>
                                    </div>
                                    <div class="row mt-4 justify-content-center">
                                        <div class="col-lg-6">
                                            <?php if ($insurance_type != 'comercial') { ?>
                                                <div class="form-group">
                                                    <label for="dni">DNI</label>
                                                    <input type="tel"
                                                           v-model="form.dni"
                                                           v-validate="'required|numeric|max:8|min:7'"
                                                           id="dni"
                                                           name="dni"
                                                           class="form-control"
                                                           data-vv-name="DNI"
                                                           :class="{ 'is-invalid': errors.has('DNI') }"/>
                                                    <div v-if="errors.has('DNI')" class="invalid-feedback">
                                                        {{ errors.first('DNI') }}
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="form-group">
                                                    <label for="dni">CUIT</label>
                                                    <input type="text"
                                                           v-model="form.cuit"
                                                           v-validate="'required'"
                                                           id="cuit"
                                                           name="cuit"
                                                           v-mask="'##-########-#'"
                                                           class="form-control"
                                                           data-vv-name="CUIT"
                                                           :class="{ 'is-invalid': errors.has('CUIT') }"/>
                                                    <div v-if="errors.has('CUIT')" class="invalid-feedback">
                                                        {{ errors.first('CUIT') }}
                                                    </div>
                                                </div>
                                            <?php } ?>
                                                <button class="btn btn-primary btn-lg px-4"
                                                        @click.stop.prevent="goToStep(3)">
                                                    SIGUIENTE
                                                </button>
                                        </div>
                                    </div>

                                </div>
                            </transition>


                            <!-- SINO LE PREGUNTAMOS QUE QUIERE -->
                            <?php /* ?>
                            <transition name="fade">
                                <!-- COTIZACIÓN -->
                                <div class="form-block" v-if="step == 3">
                                    <div class="seller-text">
                                        <h2><?php if ($seller_gender == 'fem') {
                                                echo 'Encantada';
                                            } else {
                                                echo 'Encantado';
                                            } ?> {{form.name}}</h2>
                                        <p class="lead">
                                            <big>
                                                ¿Qué tipo de <?php echo $insurance_name ?>
                                                necesitás?
                                            </big>
                                        </p>
                                    </div>

                                    <?php
                                    $args = array(
                                        'numberposts' => -1,
                                        'post_type' => 'cobertura',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'cobertura_tax',
                                                'field' => 'term_id',
                                                'terms' => $insurance,
                                            )
                                        )
                                    );
                                    $coverages = get_posts($args); ?>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="insurance">Elegí una opción</label>
                                                <select class="form-control" id="insurance" v-model="form.insurance">
                                                    <?php
                                                    foreach ($coverages as $coverage) { ?>
                                                        <option value="<?php echo $coverage->post_title ?>">
                                                            <?php echo $coverage->post_title; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center form-block-actions">
                                        <button class="btn btn-primary btn-lg px-4"
                                                @click.stop.prevent="goToStep('2')">
                                            SIGUIENTE
                                        </button>

                                    </div>
                                </div>
                            </transition>
                            <?php */ ?>

                            <!-- SI LA COBERTURA TIENE MÁS DE UNA OPCIÓN -->

                            <?php /*if (is_array(get_price($post_id))){ ?>
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'price_select'">
                                    <div class="seller-text">
                                        <h2>¿Qué tipo de cobertura necesitás?</h2>
                                    </div>

                                    <?php
                                    $coverages_prices= get_price($post_id);
                                    ?>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="insurance">Elegí una opción</label>
                                                <div class="customradio">
                                                <?php $i=0; ?>
                                               <?php  foreach ($coverages_prices as $key => $value) { ?>

                                                    <?php

                                                        if ($key=='list-prices-'.$i.'_list_price_name'){
                                                            $name = 'list-prices-'.$i.'_list_price_name';
                                                        }

                                                   ?>

                                                    <div class="customradio-primary"
                                                         :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                        <input type="radio"
                                                               name="radio_group"
                                                               id="radio_1"
                                                               value="whatsapp"
                                                               v-model="form.contactMode"
                                                               v-validate="'required'"
                                                               data-vv-name="modo de contacto"
                                                        />
                                                        <label for="radio_1"></label>

                                                        <div v-if="errors.has('número de Whatsapp')"
                                                             class="invalid-feedback">
                                                            {{ errors.first('número de Whatsapp') }}
                                                        </div>
                                                    </div>




                                                <?php echo $key.': '.$value; ?><br>
                                                <?php } ?>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="text-center form-block-actions">
                                        <button class="btn btn-primary btn-lg px-4"
                                                @click.stop.prevent="goToStep('dni')">
                                            SIGUIENTE
                                        </button>
                                    </div>
                                </div>
                            </transition>
                            <?php } */ ?>

                            <!-- CONTACTO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 3">
                                    <div class="seller-text">
                                        <h2>¿Cómo preferís que te contactemos?</h2>
                                    </div>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="customradio">
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_1"
                                                           value="whatsapp"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_1">Whatsapp</label>
                                                    <input type="text"
                                                           name="contact-whatsapp"
                                                           class="form-control"
                                                           v-if="form.contactMode=='whatsapp'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de Whatsapp"
                                                           :class="{ 'is-invalid': errors.has('número de Whatsapp') }"
                                                    >
                                                    <div v-if="errors.has('número de Whatsapp')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de Whatsapp') }}
                                                    </div>

                                                </div>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           name="radio_group"
                                                           id="radio_2"
                                                           value="phone"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_2">Teléfono</label>
                                                    <input type="text"
                                                           name="contact-phone"
                                                           class="form-control"
                                                           v-if="form.contactMode=='phone'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá el número"
                                                           v-validate="'required|numeric'"
                                                           data-vv-name="número de teléfono"
                                                           :class="{ 'is-invalid': errors.has('número de teléfono') }"
                                                    >
                                                    <div v-if="errors.has('número de teléfono')"
                                                         class="invalid-feedback">
                                                        {{ errors.first('número de teléfono') }}
                                                    </div>
                                                </div>
                                                <div class="customradio-primary"
                                                     :class="{ 'is-invalid': errors.has('modo de contacto') }">
                                                    <input type="radio"
                                                           id="radio_3"
                                                           name="radio_group"
                                                           value="email"
                                                           v-model="form.contactMode"
                                                           v-validate="'required'"
                                                           data-vv-name="modo de contacto"
                                                    />
                                                    <label for="radio_3">Email</label>

                                                    <input type="email"
                                                           name="contact-email"
                                                           class="form-control"
                                                           v-if="form.contactMode=='email'"
                                                           v-model="form.contactData"
                                                           placeholder="Ingresá tu email"
                                                           v-validate="'required|email'"
                                                           data-vv-name="email"
                                                           :class="{ 'is-invalid': errors.has('email') }"
                                                    >
                                                    <div v-if="errors.has('email')" class="invalid-feedback">
                                                        {{ errors.first('email') }}
                                                    </div>
                                                </div>
                                                <div v-if="errors.has('modo de contacto')"
                                                     class="d-block invalid-feedback">
                                                    {{ errors.first('modo de contacto') }}
                                                </div>
                                            </div>
                                            <button class="btn btn-primary btn-lg px-4"
                                                    @click.stop.prevent="goToStep(4)">
                                                SIGUIENTE
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <?php if ($price != "") { ?>
                                <transition name="fade">
                                    <div class="form-block" v-if="step == 4">
                                        <div class="seller-text">
                                            <h2>¿Cómo te gustaría pagar la póliza?</h2>
                                        </div>
                                        <div class="row mb-2 justify-content-center">
                                            <div class="col-lg-6">
                                                <div class="mx-auto">
                                                    <h4><?php echo $insurance_name ?></h4>
                                                    <?php if ($opt != "") { ?>
                                                        <p class="text-uppercase mb-0">
                                                            <strong>
                                                                <?php echo $price[1]; ?>
                                                            </strong>
                                                        </p>
                                                        <h2 class="card-price text-center">
                                                            <?php echo set_price_format($price[0]); ?>
                                                        </h2>
                                                    <?php } else { ?>
                                                        <h2 class="card-price text-center">
                                                            <?php echo set_price_format($price); ?>
                                                        </h2>
                                                    <?php } ?>
                                                </div>


                                                <div class="customradio">
                                                    <div class="customradio-primary"
                                                         :class="{ 'is-invalid': errors.has('modo de pago') }">
                                                        <input type="radio"
                                                               id="radio_payment_1"
                                                               name="radio_payment"
                                                               value="online"
                                                               v-model="form.payment"
                                                               v-validate="'required'"
                                                               data-vv-name="modo de pago"
                                                        />
                                                        <label for="radio_payment_1">Pago online</label>
                                                    </div>
                                                    <small id="pago-ayuda" class="form-text text-muted"
                                                           v-if="form.payment=='online'">Te derivaremos a la página
                                                        de
                                                        pago</small>

                                                    <div class="customradio-primary"
                                                         :class="{ 'is-invalid': errors.has('modo de pago') }">
                                                        <input type="radio"
                                                               id="radio_payment_2"
                                                               name="radio_payment"
                                                               value="oficina"
                                                               v-model="form.payment"
                                                               v-validate="'required'"
                                                               data-vv-name="modo de pago"
                                                        />
                                                        <label for="radio_payment_2">Pagar en nuestras oficinas</label>
                                                    </div>

                                                    <div class="customradio-primary"
                                                         :class="{ 'is-invalid': errors.has('modo de pago') }">
                                                        <input type="radio"
                                                               id="radio_payment_3"
                                                               name="radio_payment"
                                                               value="otro"
                                                               v-model="form.payment"
                                                               v-validate="'required'"
                                                               data-vv-name="modo de pago"
                                                        />
                                                        <label for="radio_payment_3">Pagar luego</label>
                                                    </div>

                                                    <div v-if="errors.has('modo de pago')"
                                                         class="d-block invalid-feedback">
                                                        {{ errors.first('modo de pago') }}
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary btn-lg px-4"
                                                        @click.stop.prevent="goToStep('submit')">
                                                    <i class="fas fa-circle-notch fa-spin" v-if="loading"></i> SIGUIENTE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </transition>
                            <?php } else { ?>
                                <transition name="fade">
                                    <div class="form-block" v-if="step == 4">
                                        <div class="seller-text">
                                            <h2>Gracias {{form.name}}</h2>
                                            <p class="lead" v-if="form.contactMode=='phone'">Me voy a comunicar con vos
                                                al teléfono que me dejaste lo antes posible</p>
                                            <p class="lead" v-else>Me voy a comunicar con vos por el
                                                {{form.contactMode}} que me dejaste lo antes posible</p>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6">
                                                <div class="text-center form-block-actions">
                                                    <a href="<?php echo home_url(); ?>"
                                                       class="btn btn-primary btn-block btn-lg px-4">
                                                        LISTO
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </transition>
                            <?php } ?>

                            <!-- OFICINAS -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'success-oficina'">
                                    <div class="seller-text">
                                        <h2>Genial {{form.name}}</h2>
                                        <p class="lead">¡Ya falta menos! Me voy a estar comunicando cuanto antes.</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'success-mp'">
                                    <div class="seller-text">
                                        <h2>{{form.name}} te derivaremos a la página de pago</h2>
                                        <p class="lead">Una vez realizado, volverás al sitio</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <button class="btn btn-primary btn-lg px-4" disabled="">
                                                <i class="fas fa-circle-notch fa-spin"></i> REDIRECCIONANDO
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO SUCCESS -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'success-pago'">
                                    <div class="seller-text">
                                        <h2>¡Gracias! Ya recibimos tu pago</h2>
                                        <p class="lead">Nos comunicaremos con vos para darle seguimiento</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'error-pago'">
                                    <div class="seller-text">
                                        <h2>Lo siento {{form.name}}, parece que hubo un error con la página de pago</h2>
                                        <p class="lead">Nos comunicaremos con vos para darle seguimiento</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>

                            <!-- PAGO -->
                            <transition name="fade">
                                <div class="form-block" v-if="step == 'error'">
                                    <div class="seller-text">
                                        <h2>Lo siento {{form.name}}, parece que hubo un error</h2>
                                        <p class="lead">Por favor intentá nuevamente más tarde</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="text-center form-block-actions">
                                                <a href="<?php echo home_url(); ?>"
                                                   class="btn btn-primary btn-block btn-lg px-4">
                                                    LISTO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition>


                            <input type="hidden" name="error" v-if="form.price == '' || form.insurance ==''"
                                   v-model="form.error" value="No se pudo realizar el pago.">

                            <button class="btn btn-link" @click="prev()" v-if="step > 1"><i
                                        class="fas fa-arrow-left"></i> Anterior
                            </button>
                        </form>

                        <?php
                        $postUrl = "https://checkout.payulatam.com/ppp-web-gateway-payu/";
                        $apiKey = 'XalzjNT8MikzCHR9452Piti1F1';
                        $merchantId = '781256';
                        $accountId = '788066';
                        $amount = $price[0];
                        $responseUrl = 'http://woranz.com/pago/?error=response';
                        $confirmationUrl = 'http://woranz.com/pago/?error=confirmacion';
                        ?>
                        <form method="post" id="payu" action="<?php print($postUrl) ?>">
                            <input name="oldPrice" type="hidden" value="<?php print($amount) ?>">
                            <input name="merchantId" type="hidden" value="<?php print($merchantId) ?>">
                            <input name="accountId" type="hidden" value="<?php print($accountId) ?>">
                            <input name="description" type="hidden" value="<?php print($insurance_name) ?>">
                            <input name="referenceCode" type="hidden" value="<?php print($insurance_name) ?>">
                            <input name="amount" type="hidden" :value="form.price">
                            <input name="tax" type="hidden" value="0">
                            <input name="taxReturnBase" type="hidden" value="0">
                            <input name="currency" type="hidden" value="ARS">
                            <input name="signature" type="hidden"
                                   value="<?php print(md5($apiKey . "~" . $merchantId . "~" . $insurance_name . "~" . $amount . "~ARS")) ?>">
                            <input name="buyerEmail" type="hidden" v-if="form.contactMode=='email'"
                                   :value="form.contactData">
                            <input name="buyerFullName" type="hidden" :value="form.name + ' ' + form.surname">
                            <input name="responseUrl" type="hidden" value="<?php print($responseUrl); ?>">
                            <input name="confirmationUrl" type="hidden" value="<?php print($confirmationUrl); ?>">
                        </form>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- .row -->
</div><!-- #content -->

<?php get_footer(); ?>
<script>
    Vue.use(VeeValidate);
    VeeValidate.Validator.localize("es");

    const app = new Vue({
        el: 'main',
        data: {
            step: '1',
            success: false,
            loading: false,
            form: {
                name: '',
                surname: '',
                dni: '',
                cuit: '',
                insurance: '',
                payment: '',
                price: '',
                contactMode: '',
                contactData: '',
                error: '',
                seller: '',
                seller_email: ''
            },
        },
        methods: {
            goBack() {
                window.history.back();
            },
            goToStep(step) {
                this.$validator.validate().then(valid => {
                    if (valid) {
                        if (step == 'submit') {
                            this.submit()
                        } else if (step == '4' && this.form.price == "") {
                            this.submit()
                            this.step = step;
                        } else {
                            this.step = step;
                        }
                    }
                })
            },
            prev() {
                this.step--;
            },
            next() {
                this.step++;
            },
            submit: async function () {
                this.loading = true;
                try {
                    axios.post('http://woranz.com/bin/pay-send.php', this.form);
                    this.loading = false;
                    if (this.form.payment == 'oficina' || this.form.payment == 'otro' ) {
                        this.step = 'success-oficina'
                    }
                    if (this.form.payment == 'online') {
                        if (this.form.insurance != "" && this.form.price != "") {
                            document.getElementById("payu").submit();
                            this.step = 'success-mp'
                        } else {
                            this.step = '4'
                        }
                    }
                } catch (e) {
                    this.loading = false;
                    if (this.form.payment == 'online') {
                        this.step = 'error-pago';
                    }else{
                        this.step = 'error';
                    }
                }
            },
            toFormData: function (obj) {
                let formData = new FormData();
                for (let key in obj) {
                    formData.append(key, obj[key]);
                }
                return formData;
            }
        },
        beforeMount: function () {
            this.form.insurance = this.$el.querySelector('input[name=insurance_selected]').value;
            this.form.price = this.$el.querySelector('input[name=price]').value;
            this.form.seller = this.$el.querySelector('input[name=seller]').value;
            this.form.seller_email = this.$el.querySelector('input[name=seller_email]').value;

            if (this.$el.querySelector('input[name=get_error]').value == 'error'){
                this.step = 'error-pago';
            }
            if (this.$el.querySelector('input[name=get_error]').value == 'confirmacion'){
                this.step = 'success-pago';
            }
            console.log(this.form);
        }
    });
</script>
